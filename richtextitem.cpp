﻿#include "ntlx/richtextitem.h"

#include "ntlx/note.h"
#include "ntlx/lmbcs.h"

namespace ntlx
{

RichTextItem::RichTextItem()
  : Item()
{
}

QString RichTextItem::cdTypeName(WORD cdType)
{
  switch (cdType)
  {
  case SIG_CD_PDEF_MAIN: return "PDEF_MAIN"; break;
  case SIG_CD_PDEF_TYPE: return "PDEF_TYPE"; break;
  case SIG_CD_PDEF_PROPERTY: return "PDEF_PROPERTY"; break;
  case SIG_CD_PDEF_ACTION: return "PDEF_ACTION"; break;
  case SIG_CD_TABLECELL_DATAFLAGS: return "TABLECELL_DATAFLAGS"; break;
  case SIG_CD_EMBEDDEDCONTACTLIST: return "EMBEDDEDCONTACTLIST"; break;
  case SIG_CD_IGNORE: return "IGNORE"; break;
  case SIG_CD_TABLECELL_HREF2: return "TABLECELL_HREF2"; break;
  case SIG_CD_HREFBORDER: return "HREFBORDER"; break;
  case SIG_CD_TABLEDATAEXTENSION: return "TABLEDATAEXTENSION"; break;
  case SIG_CD_EMBEDDEDCALCTL: return "EMBEDDEDCALCTL"; break;
  case SIG_CD_ACTIONEXT: return "ACTIONEXT"; break;
  case SIG_CD_EVENT_LANGUAGE_ENTRY: return "EVENT_LANGUAGE_ENTRY"; break;
  case SIG_CD_FILESEGMENT: return "FILESEGMENT"; break;
  case SIG_CD_FILEHEADER: return "FILEHEADER"; break;
  case SIG_CD_DATAFLAGS: return "DATAFLAGS"; break;
  case SIG_CD_BACKGROUNDPROPERTIES: return "BACKGROUNDPROPERTIES"; break;
  case SIG_CD_EMBEDEXTRA_INFO: return "EMBEDEXTRA_INFO"; break;
  case SIG_CD_CLIENT_BLOBPART: return "CLIENT_BLOBPART"; break;
  case SIG_CD_CLIENT_EVENT: return "CLIENT_EVENT"; break;
  case SIG_CD_BORDERINFO_HS: return "BORDERINFO_HS"; break;
  case SIG_CD_LARGE_PARAGRAPH: return "LARGE_PARAGRAPH"; break;
  case SIG_CD_EXT_EMBEDDEDSCHED: return "EXT_EMBEDDEDSCHED"; break;
  case SIG_CD_BOXSIZE: return "BOXSIZE"; break;
  case SIG_CD_POSITIONING: return "POSITIONING"; break;
  case SIG_CD_LAYER: return "LAYER"; break;
  case SIG_CD_DECSFIELD: return "DECSFIELD"; break;
  case SIG_CD_SPAN_END: return "SPAN_END"; break;
  case SIG_CD_SPAN_BEGIN: return "SPAN_BEGIN"; break;
  case SIG_CD_TEXTPROPERTIESTABLE: return "TEXTPROPERTIESTABLE"; break;
  case SIG_CD_HREF2: return "HREF2"; break;
  case SIG_CD_BACKGROUNDCOLOR: return "BACKGROUNDCOLOR"; break;
  case SIG_CD_INLINE: return "INLINE"; break;
  case SIG_CD_V6HOTSPOTBEGIN_CONTINUATION: return "V6HOTSPOTBEGIN_CONTINUATION"; break;
  case SIG_CD_TARGET_DBLCLK: return "TARGET_DBLCLK"; break;
  case SIG_CD_CAPTION: return "CAPTION"; break;
  case SIG_CD_LINKCOLORS: return "LINKCOLORS"; break;
  case SIG_CD_TABLECELL_HREF: return "TABLECELL_HREF"; break;
  case SIG_CD_ACTIONBAREXT: return "ACTIONBAREXT"; break;
  case SIG_CD_IDNAME: return "IDNAME"; break;
  case SIG_CD_TABLECELL_IDNAME: return "TABLECELL_IDNAME"; break;
  case SIG_CD_IMAGESEGMENT: return "IMAGESEGMENT"; break;
  case SIG_CD_IMAGEHEADER: return "IMAGEHEADER"; break;
  case SIG_CD_V5HOTSPOTBEGIN: return "V5HOTSPOTBEGIN"; break;
  case SIG_CD_V5HOTSPOTEND: return "V5HOTSPOTEND"; break;
  case SIG_CD_TEXTPROPERTY: return "TEXTPROPERTY"; break;
  case SIG_CD_PARAGRAPH: return "PARAGRAPH"; break;
  case SIG_CD_PABDEFINITION: return "PABDEFINITION"; break;
  case SIG_CD_PABREFERENCE: return "PABREFERENCE"; break;
  case SIG_CD_TEXT: return "TEXT"; break;
  case SIG_CD_HEADER: return "HEADER"; break;
  case SIG_CD_LINKEXPORT2: return "LINKEXPORT2"; break;
  case SIG_CD_BITMAPHEADER: return "BITMAPHEADER"; break;
  case SIG_CD_BITMAPSEGMENT: return "BITMAPSEGMENT"; break;
  case SIG_CD_COLORTABLE: return "COLORTABLE"; break;
  case SIG_CD_GRAPHIC: return "GRAPHIC"; break;
  case SIG_CD_PMMETASEG: return "PMMETASEG"; break;
  case SIG_CD_WINMETASEG: return "WINMETASEG"; break;
  case SIG_CD_MACMETASEG: return "MACMETASEG"; break;
  case SIG_CD_CGMMETA: return "CGMMETA"; break;
  case SIG_CD_PMMETAHEADER: return "PMMETAHEADER"; break;
  case SIG_CD_WINMETAHEADER: return "WINMETAHEADER"; break;
  case SIG_CD_MACMETAHEADER: return "MACMETAHEADER"; break;
  case SIG_CD_TABLEBEGIN: return "TABLEBEGIN"; break;
  case SIG_CD_TABLECELL: return "TABLECELL"; break;
  case SIG_CD_TABLEEND: return "TABLEEND"; break;
  case SIG_CD_STYLENAME: return "STYLENAME"; break;
  case SIG_CD_STORAGELINK: return "STORAGELINK"; break;
  case SIG_CD_TRANSPARENTTABLE: return "TRANSPARENTTABLE"; break;
  case SIG_CD_HORIZONTALRULE: return "HORIZONTALRULE"; break;
  case SIG_CD_ALTTEXT: return "ALTTEXT"; break;
  case SIG_CD_ANCHOR: return "ANCHOR"; break;
  case SIG_CD_HTMLBEGIN: return "HTMLBEGIN"; break;
  case SIG_CD_HTMLEND: return "HTMLEND"; break;
  case SIG_CD_HTMLFORMULA: return "HTMLFORMULA"; break;
  case SIG_CD_NESTEDTABLEBEGIN: return "NESTEDTABLEBEGIN"; break;
  case SIG_CD_NESTEDTABLECELL: return "NESTEDTABLECELL"; break;
  case SIG_CD_NESTEDTABLEEND: return "NESTEDTABLEEND"; break;
  case SIG_CD_COLOR: return "COLOR"; break;
  case SIG_CD_TABLECELL_COLOR: return "TABLECELL_COLOR"; break;
  case SIG_CD_BLOBPART: return "BLOBPART"; break;
  case SIG_CD_BEGIN: return "BEGIN"; break;
  case SIG_CD_END: return "END"; break;
  case SIG_CD_VERTICALALIGN: return "VERTICALALIGN"; break;
  case SIG_CD_FLOATPOSITION: return "FLOATPOSITION"; break;
  case SIG_CD_TIMERINFO: return "TIMERINFO"; break;
  case SIG_CD_TABLEROWHEIGHT: return "TABLEROWHEIGHT"; break;
  case SIG_CD_TABLELABEL: return "TABLELABEL"; break;
  case SIG_CD_BIDI_TEXT: return "BIDI_TEXT"; break;
  case SIG_CD_BIDI_TEXTEFFECT: return "BIDI_TEXTEFFECT"; break;
  case SIG_CD_REGIONBEGIN: return "REGIONBEGIN"; break;
  case SIG_CD_REGIONEND: return "REGIONEND"; break;
  case SIG_CD_TRANSITION: return "TRANSITION"; break;
  case SIG_CD_FIELDHINT: return "FIELDHINT"; break;
  case SIG_CD_PLACEHOLDER: return "PLACEHOLDER"; break;
  case SIG_CD_EMBEDDEDOUTLINE: return "EMBEDDEDOUTLINE"; break;
  case SIG_CD_EMBEDDEDVIEW: return "EMBEDDEDVIEW"; break;
  case SIG_CD_CELLBACKGROUNDDATA: return "CELLBACKGROUNDDATA"; break;
  case SIG_CD_FRAMESETHEADER: return "FRAMESETHEADER"; break;
  case SIG_CD_FRAMESET: return "FRAMESET"; break;
  case SIG_CD_FRAME: return "FRAME"; break;
  case SIG_CD_TARGET: return "TARGET"; break;
  case SIG_CD_MAPELEMENT: return "MAPELEMENT"; break;
  case SIG_CD_AREAELEMENT: return "AREAELEMENT"; break;
  case SIG_CD_HREF: return "HREF"; break;
  case SIG_CD_EMBEDDEDCTL: return "EMBEDDEDCTL"; break;
  case SIG_CD_HTML_ALTTEXT: return "HTML_ALTTEXT"; break;
  case SIG_CD_EVENT: return "EVENT"; break;
  case SIG_CD_PRETABLEBEGIN: return "PRETABLEBEGIN"; break;
  case SIG_CD_BORDERINFO: return "BORDERINFO"; break;
  case SIG_CD_EMBEDDEDSCHEDCTL: return "EMBEDDEDSCHEDCTL"; break;
  case SIG_CD_EXT2_FIELD: return "EXT2_FIELD"; break;
  case SIG_CD_EMBEDDEDEDITCTL: return "EMBEDDEDEDITCTL"; break;
  case SIG_CD_DOCUMENT_PRE_26: return "DOCUMENT_PRE_26"; break;
  case SIG_CD_FIELD_PRE_36: return "FIELD_PRE_36"; break;
  case SIG_CD_FIELD: return "FIELD"; break;
  case SIG_CD_DOCUMENT: return "DOCUMENT"; break;
  case SIG_CD_METAFILE: return "METAFILE"; break;
  case SIG_CD_BITMAP: return "BITMAP"; break;
  case SIG_CD_FONTTABLE: return "FONTTABLE"; break;
  case SIG_CD_LINK: return "LINK"; break;
  case SIG_CD_LINKEXPORT: return "LINKEXPORT"; break;
  case SIG_CD_KEYWORD: return "KEYWORD"; break;
  case SIG_CD_LINK2: return "LINK2"; break;
  case SIG_CD_CGM: return "CGM"; break;
  case SIG_CD_TIFF: return "TIFF"; break;
  case SIG_CD_PATTERNTABLE: return "PATTERNTABLE"; break;
  case SIG_CD_DDEBEGIN: return "DDEBEGIN"; break;
  case SIG_CD_DDEEND: return "DDEEND"; break;
  case SIG_CD_OLEBEGIN: return "OLEBEGIN"; break;
  case SIG_CD_OLEEND: return "OLEEND"; break;
  case SIG_CD_HOTSPOTBEGIN: return "HOTSPOTBEGIN"; break;
  case SIG_CD_HOTSPOTEND: return "HOTSPOTEND"; break;
  case SIG_CD_BUTTON: return "BUTTON"; break;
  case SIG_CD_BAR: return "BAR"; break;
  case SIG_CD_V4HOTSPOTBEGIN: return "V4HOTSPOTBEGIN"; break;
  case SIG_CD_V4HOTSPOTEND: return "V4HOTSPOTEND"; break;
  case SIG_CD_EXT_FIELD: return "EXT_FIELD"; break;
  case SIG_CD_LSOBJECT: return "LSOBJECT"; break;
  case SIG_CD_HTMLHEADER: return "HTMLHEADER"; break;
  case SIG_CD_HTMLSEGMENT: return "HTMLSEGMENT"; break;
  case SIG_CD_LAYOUT: return "LAYOUT"; break;
  case SIG_CD_LAYOUTTEXT: return "LAYOUTTEXT"; break;
  case SIG_CD_LAYOUTEND: return "LAYOUTEND"; break;
  case SIG_CD_LAYOUTFIELD: return "LAYOUTFIELD"; break;
  case SIG_CD_PABHIDE: return "PABHIDE"; break;
  case SIG_CD_PABFORMREF: return "PABFORMREF"; break;
  case SIG_CD_ACTIONBAR: return "ACTIONBAR"; break;
  case SIG_CD_ACTION: return "ACTION"; break;
  case SIG_CD_DOCAUTOLAUNCH: return "DOCAUTOLAUNCH"; break;
  case SIG_CD_LAYOUTGRAPHIC: return "LAYOUTGRAPHIC"; break;
  case SIG_CD_OLEOBJINFO: return "OLEOBJINFO"; break;
  case SIG_CD_LAYOUTBUTTON: return "LAYOUTBUTTON"; break;
  case SIG_CD_TEXTEFFECT: return "TEXTEFFECT"; break;
  case SIG_CD_VMHEADER: return "VMHEADER"; break;
  case SIG_CD_VMBITMAP: return "VMBITMAP"; break;
  case SIG_CD_VMRECT: return "VMRECT"; break;
  case SIG_CD_VMPOLYGON_BYTE: return "VMPOLYGON_BYTE"; break;
  case SIG_CD_VMPOLYLINE_BYTE: return "VMPOLYLINE_BYTE"; break;
  case SIG_CD_VMREGION: return "VMREGION"; break;
  case SIG_CD_VMACTION: return "VMACTION"; break;
  case SIG_CD_VMELLIPSE: return "VMELLIPSE"; break;
//  case SIG_CD_VMRNDRECT: return "VMRNDRECT"; break;
//  case SIG_CD_VMBUTTON: return "VMBUTTON"; break;
  case SIG_CD_VMACTION_2: return "VMACTION_2"; break;
//  case SIG_CD_VMTEXTBOX: return "VMTEXTBOX"; break;
  case SIG_CD_VMPOLYGON: return "VMPOLYGON"; break;
  case SIG_CD_VMPOLYLINE: return "VMPOLYLINE"; break;
//  case SIG_CD_VMPOLYRGN: return "VMPOLYRGN"; break;
  case SIG_CD_VMCIRCLE: return "VMCIRCLE"; break;
//  case SIG_CD_VMPOLYRGN_BYTE: return "VMPOLYRGN_BYTE"; break;
  case SIG_CD_ALTERNATEBEGIN: return "ALTERNATEBEGIN"; break;
  case SIG_CD_ALTERNATEEND: return "ALTERNATEEND"; break;
  case SIG_CD_OLERTMARKER: return "OLERTMARKER"; break;
  }
  return "**Unknown**";
}

Status RichTextItem::enumerate(
    Note& note
    , const QString& name
    , ActionRoutinePtr callback
    , void* contextPtr
    )
{
  note.open();
  if (note.lastStatus().failure())
    return note.lastStatus();

  NOTEHANDLE handle = note;
  Lmbcs lmbcsName = Lmbcs::fromQString(name);
  BLOCKID bidItem, bidPrev, bidValue;
  WORD type = TYPE_INVALID_OR_UNKNOWN;
  DWORD valueLen = 0;
  Status result = NSFItemInfo(
        handle
        , lmbcsName.constData()
        , lmbcsName.size()
        , &bidItem
        , &type
        , &bidValue
        , &valueLen
        );
  while (result.success() && type == TYPE_COMPOSITE)
  {
    result = EnumCompositeBuffer(bidValue, valueLen, callback, contextPtr);
    bidPrev = bidItem;
    result = NSFItemInfoNext(
          handle
          , bidPrev
          , lmbcsName.constData()
          , lmbcsName.size()
          , &bidItem
          , 0
          , &bidValue
          , &valueLen
          );
  }
  return result;
}

} // namespace ntlx
