﻿#include "ntlx/timedate.h"
#include "ntlx/lmbcs.h"

#include <QTimeZone>
#include <QVariant>

namespace ntlx
{

TimeDate::TimeDate()
{
  TimeConstant(TIMEDATE_WILDCARD, &value_);
}

TimeDate::TimeDate(const TIMEDATE& value)
  : value_(value)
{
}

TimeDate::TimeDate(const TimeDate &other)
  : value_(other.value_)
{
}

TimeDate& TimeDate::operator =(const TimeDate& other)
{
  if (this == &other) return *this;
  value_ = other.value_;
  return *this;
}

QDateTime TimeDate::toQDateTime() const
{
  TIME time;
  time.GM = value_;
  TimeGMToLocal(&time);
  QDate qDate(time.year, time.month, time.day);
  QTime qTime(time.hour, time.minute, time.second, time.hundredth * 10);
  return QDateTime(qDate, qTime, Qt::LocalTime, time.zone * -3600);
}

QVariant TimeDate::toVariant() const
{
  return QVariant(toQDateTime());
}

TimeDate TimeDate::current()
{
  TimeDate td;
  OSCurrentTIMEDATE(&td.value_);
  return td;
}

TimeDate TimeDate::minimum()
{
  return getConstant<TIMEDATE_MINIMUM>();
}

TimeDate TimeDate::maximum()
{
  return getConstant<TIMEDATE_MAXIMUM>();
}

TimeDate TimeDate::wildcard()
{
  return getConstant<TIMEDATE_WILDCARD>();
}

Lmbcs TimeDate::toLmbcs(
    void* intlFormat
    , TFMT* textFormat
    , Status* status
    ) const
{
  char text[MAXALPHATIMEDATE];
  WORD len = 0;
  Status result = ConvertTIMEDATEToText(
        intlFormat
        , textFormat
        , &value_
        , text
        , MAXALPHATIMEDATE
        , &len
        );
  Q_ASSERT(len <= MAXALPHATIMEDATE);
  if (status != nullptr) *status = result;
  return Lmbcs(text, len);
}

QString TimeDate::toString(
    void* intlFormat
    , TFMT* textFormat
    , Status* status
    ) const
{
  return toLmbcs(intlFormat, textFormat, status).toQString();
}

TimeDate TimeDate::fromQDateTime(const QDateTime& qDateTime)
{
  TIME time;
  QDate qDate = qDateTime.date();
  QTime qTime = qDateTime.time();
  time.zone = qDateTime.offsetFromUtc() / -3600;
  time.dst = qDateTime.isDaylightTime() ? 1 : 0;
  time.year = qDate.year();
  time.month = qDate.month();
  time.day = qDate.day();
  time.weekday = qDate.dayOfWeek() == 7 ? 1 : (qDate.dayOfWeek() + 1);
  time.hour = qTime.hour();
  time.minute = qTime.minute();
  time.second = qTime.second();
  time.hundredth = qTime.msec() / 10;
  TimeLocalToGM(&time);
  return TimeDate(time.GM);
}

TimeDate TimeDate::fromQtData(const QDateTime &from)
{
  return fromQDateTime(from);
}

TimeDate TimeDate::fromLmbcs(
    Lmbcs &lmbcs
    , void* intlFormat
    , TFMT* textFormat
    , Status* status
    )
{
  TimeDate td;
  char *p = lmbcs.data();
  Status result = ConvertTextToTIMEDATE(
        intlFormat
        , textFormat
        , &p
        , lmbcs.size() < MAXALPHATIMEDATE ? lmbcs.size() : MAXALPHATIMEDATE
        , &td.value_
        );
  if (status != nullptr) *status = result;
  return td;
}

TimeDate TimeDate::fromVariant(const QVariant& var)
{
  return fromQDateTime(var.toDateTime());
}

} // namespace ntlx
