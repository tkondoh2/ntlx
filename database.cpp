﻿#include "ntlx/database.h"
#include "ntlx/netpath.h"
#include "ntlx/pathset.h"
#include "ntlx/dbinfo.h"
#include "ntlx/idtable.h"
#include "ntlx/note.h"
#include "ntlx/timedate.h"

#include <QtDebug>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfsearc.h>
#include <nsfnote.h>
#include <nsfdata.h>
#include <nif.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace ntlx
{

Database::Database()
  : IStatus()
  , handle_(NULLHANDLE)
{
}

Database::Database(const PathSet& pathSet)
  : IStatus()
  , handle_(NULLHANDLE)
{
  open(pathSet);
}

Database::Database(const NetPath& netPath)
  : IStatus()
  , handle_(NULLHANDLE)
{
  open(netPath);
}

Database::~Database()
{
  close();
}

DbInfo Database::getInfo() const
{
  Q_ASSERT(handle());
  char infoData[NSF_INFO_SIZE];
  lastStatus_ = NSFDbInfoGet(handle(), infoData);
  return lastStatus_.success() ? DbInfo(infoData) : DbInfo();
}

QPair<TimeDate, TimeDate> Database::getModifiedPair()
{
  Q_ASSERT(handle());
  TimeDate modDoc, modDesign;
  lastStatus_ = NSFDbModifiedTime(
        handle()
        , &modDoc.value_
        , &modDesign.value_
        );
  if (lastStatus_.success())
    return QPair<TimeDate, TimeDate>(modDoc, modDesign);
  else
    return QPair<TimeDate, TimeDate>(TimeDate(), TimeDate());
}

Database& Database::open(const PathSet& pathSet)
{
  return open(pathSet.toNetPath());
}

Database& Database::open(const NetPath& netPath)
{
  if (close().lastStatus().failure())
    return *this;

  lastStatus_ = NSFDbOpen(netPath.constData(), &handle_);
  if (lastStatus_.failure()) handle_ = NULLHANDLE;
  return *this;
}

Database& Database::close()
{
  clearStatus();
  if (handle_ != NULLHANDLE)
  {
    lastStatus_ = NSFDbClose(handle_);
    handle_ = NULLHANDLE;
  }
  return *this;
}

/**
 * @brief Database::collectNotes用コールバック関数(IDテーブル)
 * @param ptr 汎用ポインタ
 * @param pSearchMatch 検索マッチ構造体へのポインタ
 * @param pItemTable ITEM_TABLE(サマリーバッファ)へのポインタ
 * @return 結果ステータス
 */
STATUS LNPUBLIC callback_collectNotes_idTable(
    void* ptr
    , SEARCH_MATCH* pSearchMatch
    , ITEM_TABLE* pItemTable
    );

Status Database::collectNotes(
    IDTable& idTable
    , WORD noteMask
    )
{
  Q_ASSERT(handle_);
  return NSFSearch(
        handle_
        , 0
        , 0
        , 0
        , noteMask
        , 0
        , callback_collectNotes_idTable
        , &idTable
        , 0
        );
}

/**
 * @brief Database::collectNotes用コールバック関数(サマリーバッファ)
 * @param ptr 汎用ポインタ
 * @param pSearchMatch 検索マッチ構造体へのポインタ
 * @param pItemTable ITEM_TABLE(サマリーバッファ)へのポインタ
 * @return 結果ステータス
 */
STATUS LNPUBLIC callback_collectNotes_summaryList(
    void* ptr
    , SEARCH_MATCH* pSearchMatch
    , ITEM_TABLE* pItemTable
    );

Status Database::collectNotes(
    SummaryList& summaryList
    , WORD noteMask
    )
{
  Q_ASSERT(handle_);
  return NSFSearch(
        handle_
        , 0
        , 0
        , SEARCH_SUMMARY
        , noteMask
        , 0
        , callback_collectNotes_summaryList
        , &summaryList
        , 0
        );
}

STATUS LNPUBLIC callback_collectNotes_idTable(
    void* ptr
    , SEARCH_MATCH* pSearchMatch
    , ITEM_TABLE* /*pItemTable*/
    )
{
  if (!(pSearchMatch->SERetFlags & SE_FMATCH)) return NOERROR;

  SEARCH_MATCH searchMatch;
  memcpy((char*)&searchMatch, (char*)pSearchMatch, sizeof(SEARCH_MATCH));

  IDTable* idTable = (IDTable*)ptr;
  idTable->insert(searchMatch.ID.NoteID);
  return NOERROR;
}

STATUS LNPUBLIC callback_collectNotes_summaryList(
    void* ptr
    , SEARCH_MATCH* pSearchMatch
    , ITEM_TABLE* pItemTable
    )
{
  if (!(pSearchMatch->SERetFlags & SE_FMATCH)) return NOERROR;

  SummaryMap map;
  map.load(pSearchMatch, pItemTable);

  SummaryList* list = (SummaryList*)ptr;
  list->append(map);
  return NOERROR;
}

Note Database::getDesignNote(const QString &name, WORD noteClass)
{
  NOTEID noteId = 0;
  lastStatus_ = NIFFindDesignNote(
        handle_
        , Lmbcs::fromQString(name).constData()
        , noteClass
        , &noteId
        );
  return Note(*this, noteId);
}

} // namespace ntlx
