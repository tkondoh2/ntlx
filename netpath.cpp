﻿#include "ntlx/netpath.h"
#include "ntlx/pathset.h"

#include <QtDebug>
#include "ntlx/database.h"
#include "ntlx/dbinfo.h"
#include "ntlx/timedate.h"
#include "ntlx/summarymap.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <names.h>
#include <osfile.h>
#include <stdnames.h>
#include <nsfdb.h>
#include <nsfsearc.h>
#include <nsfnote.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace ntlx
{

/**
 * @brief NetPath::collectFiles用コールバック関数
 * @param ptr 汎用ポインタ(FileMapListへのポインタ)
 * @param pSearchMatch SEARCH_MATCHへのポインタ
 * @param pItemTable ITEM_TABLEへのポインタ
 * @return 結果ステータス
 */
STATUS LNPUBLIC callback_collectFiles(
    void* ptr
    , SEARCH_MATCH* pSearchMatch
    , ITEM_TABLE* pItemTable
    );

/**
 * @brief サマリーバッファから目的のテキストデータを取得する
 * @param pItemTable ITEM_TABLEへのポインタ
 * @param name テキストデータ名
 * @param maxSize 用意するバッファサイズ
 * @return 取得したLmbcsオブジェクト
 */
Lmbcs getSummaryValue( ITEM_TABLE* pItemTable, const char* name, WORD maxSize);

NetPath::NetPath()
  : Lmbcs()
{
}

NetPath::NetPath(const char* data, int len)
  : Lmbcs(data, len)
{
}

NetPath::NetPath(const PathSet& pathSet)
  : Lmbcs(pathSet.toNetPath())
{
}

NetPath::NetPath(const NetPath &other)
  : Lmbcs(other)
{
}

NetPath& NetPath::operator=(const NetPath& other)
{
  if (this == &other) return *this;
  Lmbcs::operator=(other);
  return *this;
}

NetPath::~NetPath()
{
}

PathSet NetPath::toPathSet() const
{
  char retFile[MAXPATH], retServer[MAXPATH], retPort[MAXPATH];
  Status result = OSPathNetParse(constData(), retPort, retServer, retFile);
  return result.success()
      ? PathSet(Lmbcs(retFile).toQString()
                 , Lmbcs(retServer).toQString()
                 , Lmbcs(retPort).toQString()
                 )
      : PathSet();
}

STATUS LNPUBLIC callback_collectFiles(
    void* ptr
    , SEARCH_MATCH* /*pSearchMatch*/
    , ITEM_TABLE* pItemTable
    )
{
  SummaryList* fileMapList = static_cast<SummaryList*>(ptr);
  SummaryMap fileMap;

  fileMap.load(nullptr, pItemTable);
  fileMapList->append(fileMap);

  return NOERROR;
}

Status NetPath::collectFiles(SummaryList &list, WORD fileMask)
{
  Database db;
  if (db.open(*this).lastStatus().failure())
    return db.lastStatus();

  return NSFSearch(
        db
        , 0
        , 0
        , SEARCH_FILETYPE | SEARCH_SUMMARY
        , fileMask
        , 0
        , callback_collectFiles
        , &list
        , 0
        );
}

template <WORD maxSize>
Lmbcs getSummaryValue(ITEM_TABLE* pItemTable, const char* name)
{
  char buffer[maxSize];
  return NSFGetSummaryValue(pItemTable, name, buffer, maxSize)
      ? Lmbcs(buffer)
      : Lmbcs();
}

} // namespace ntlx
