﻿#ifndef NTLX_NOTE_H
#define NTLX_NOTE_H

#include "ntlx_global.h"
#include "ntlx/status.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfnote.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace ntlx
{

class Database;
class Formula;
class TimeDate;
class Item;

/**
 * @brief 文書クラス
 */
class NTLXSHARED_EXPORT Note
    : public IStatus
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  Note();

  /**
   * @brief コンストラクタ
   * @param db データベース
   * @param id NOTEID
   */
  Note(Database& db, NOTEID id = 0);

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元
   */
  Note(const Note& other);

  /**
   * @brief 代入演算子
   * @param other 代入元
   * @return 自身への参照
   */
  Note& operator=(const Note& other);

  /**
   * @brief デストラクタ
   */
  virtual ~Note();

  /**
   * @brief 文書のNOTEIDを割り当てる
   * @param noteId NOTEID
   * @param pDatabase データベースへのポインタ
   */
  void assign(NOTEID noteId, Database* pDatabase);

  /**
   * @brief 文書を開く
   * @param openFlags 文書オープンフラグ
   * @return 自身への参照
   */
  Note& open(WORD openFlags = 0);

  /**
   * @brief 文書を閉じる
   * @return 自身への参照
   */
  Note& close();

  /**
   * @brief アイテムのテキストを取得する(256バイトまで)
   * @param itemName アイテム名
   * @return データテキスト
   */
  QString getShortText(const QString& itemName);

  /**
   * @brief アイテムの日時データを取得する
   * @param itemName アイテム名
   * @return 日時データ
   */
  TimeDate getTimeDate(const QString& itemName);

  /**
   * @brief アイテムの日時リストデータを取得する
   * @param itemName アイテム名
   * @return 日時リストデータ
   */
  QList<TimeDate> getTimeDateList(const QString& itemName);

  /**
   * @brief アイテムの浮動小数点数データを取得する
   * @param itemName アイテム名
   * @param defaultValue データが取得できない時のデフォルト値
   * @return 浮動小数点数データ(倍精度)
   */
  double getFloat(const QString& itemName, const double defaultValue = 0.);

  /**
   * @brief アイテムの整数データを取得する
   * @param itemName アイテム名
   * @param defaultValue データが取得できない時のデフォルト値
   * @return 整数データ(long幅)
   */
  long getLongInt(const QString& itemName, const long defaultValue = 0);

  /**
   * @brief 式アイテムを取得する
   * @param itemName アイテム名
   * @return 式オブジェクト
   */
  Formula getFormula(const QString& itemName);

  /**
   * @brief ウィンドウタイトルを取得する
   * @return ウィンドウタイトル
   */
  QString getWindowTitle();

  Note& getItems(QList<Item>& items);

  operator NOTEHANDLE() const { return handle_; }

protected:
  static STATUS LNPUBLIC callback_getItems(
      WORD spare
      , WORD flags
      , char* name
      , WORD nameLength
      , void* value
      , DWORD valueLength
      , void* ptr
      );

protected:
  Database* db_;
  mutable NOTEID id_;
  mutable NOTEHANDLE handle_;

  friend class Formula;
};

} // namespace ntlx

#endif // NTLX_NOTE_H
