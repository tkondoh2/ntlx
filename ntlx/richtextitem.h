﻿#ifndef NTLX_RICHTEXTITEM_H
#define NTLX_RICHTEXTITEM_H

#include <ntlx/item.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <ods.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace ntlx
{

class Note;

class NTLXSHARED_EXPORT RichTextItem
    : public Item
{
public:
  RichTextItem();

  static QString cdTypeName(WORD type);

  static Status enumerate(
      Note& note
      , const QString& name
      , ActionRoutinePtr callback
      , void* contextPtr
      );
};

} // namespace ntlx

#endif // NTLX_RICHTEXTITEM_H
