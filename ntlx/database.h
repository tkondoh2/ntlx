﻿#ifndef NTLX_DATABASE_H
#define NTLX_DATABASE_H

#include "ntlx_global.h"
#include <QString>
#include <QMap>
#include <QVariant>
#include "ntlx/lmbcs.h"
#include "ntlx/pathset.h"
#include "ntlx/summarymap.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfdb.h>
#include <nsfnote.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace ntlx {

class DbInfo;
class IDTable;
class Note;
class TimeDate;

/**
 * @brief データベースクラス
 */
class NTLXSHARED_EXPORT Database
    : public IStatus
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  explicit Database();

  /**
   * @brief パスセットによるコンストラクタ
   * @param pathSet パスセット
   */
  explicit Database(const PathSet& pathSet);

  /**
   * @brief ネットパスによるコンストラクタ
   * @param netPath ネットパス
   */
  explicit Database(const NetPath& netPath);

  /**
   * @brief デストラクタ
   */
  virtual ~Database();

  /**
   * @brief DBHANDLEキャスト演算子
   */
  operator DBHANDLE() const { return handle_; }

  /**
   * @brief データベース情報を取得する
   * @return DbInfoオブジェクト
   */
  DbInfo getInfo() const;

  /**
   * @brief 更新日付を取得する
   * @return 文書(1st)と設計(2nd)の更新日時
   */
  QPair<TimeDate, TimeDate> getModifiedPair();

  /**
   * @brief データベースを開く
   * @param pathSet パスセット
   * @return 自身への参照
   */
  Database& open(const PathSet& pathSet);

  /**
   * @brief データベースを開く
   * @param netPath ネットパス
   * @return 自身への参照
   */
  Database& open(const NetPath& netPath);

  /**
   * @brief データベースを閉じる
   * @return 自身への参照
   */
  Database& close();

  /**
   * @brief 文書を収集する(IDテーブル)
   * @param idTable IDTableオブジェクト
   * @param noteMask 文書マスク
   * @return 結果ステータス
   */
  Status collectNotes(
      IDTable& idTable
      , WORD noteMask = NOTE_CLASS_DOCUMENT
      );

  /**
   * @brief 文書を収集する(サマリーバッファ)
   * @param map
   * @param noteMask
   * @return
   */
  Status collectNotes(
      SummaryList& summaryList
      , WORD noteMask = NOTE_CLASS_DOCUMENT
      );

  /**
   * @brief 設計文書を取得する
   * @param name 設計文書名
   * @param noteClass 設計文書クラス
   * @return 取得した設計文書
   */
  Note getDesignNote(const QString& name, WORD noteClass);

protected:
  /**
   * @brief データベースハンドルを返す
   * @return データベースハンドル
   */
  DBHANDLE handle() const { return handle_; }

  /**
   * @brief ハンドルを設定する
   * @param handle データベースハンドル
   */
  void setHandle(DBHANDLE handle) { handle_ = handle; }

private:
  DBHANDLE handle_;

  Database(const Database&);
  Database& operator=(const Database&);

  friend class Note;
};

} // namespace ntlx

#endif // NTLX_DATABASE_H
