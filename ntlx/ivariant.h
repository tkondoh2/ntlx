﻿#ifndef NTLX_IVARIANT_H
#define NTLX_IVARIANT_H

#include <QVariant>

namespace ntlx
{

template <typename T>
class IVariant
{
public:
  IVariant()
    : value_()
  {
  }

  IVariant(const T& value)
    : value_(value)
  {
  }

  IVariant& operator=(const T& value)
  {
    value_ = value;
    return *this;
  }

  IVariant(const IVariant& other)
    : value_(other.value_)
  {
  }

  IVariant& operator=(const IVariant& other)
  {
    if (this == &other) return *this;
    value_ = other.value_;
    return *this;
  }

  T data() const
  {
    return value_;
  }

  virtual QVariant toQVariant() const = 0;

  virtual void setQVariant(const QVariant& var) = 0;

  virtual QString toString(void* options = nullptr) const = 0;

protected:
  T value_;
};

} // namespace ntlx

#endif // NTLX_IVARIANT_H
