﻿#ifndef NTLX_NETPATH_H
#define NTLX_NETPATH_H

#include <QMap>
#include <QVariant>
#include <ntlx/summarymap.h>
#include <ntlx/lmbcs.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <osfile.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace ntlx
{

class Status;
class PathSet;

/**
 * @brief ネットパスクラス
 */
class NTLXSHARED_EXPORT NetPath
    : public Lmbcs
{
public:
  /**
   * @brief コンストラクタ
   */
  NetPath();

  /**
   * @brief コピー元LMBCSの文字列ポインタと長さによるコンストラクタ
   * @param コピー元LMBCSのポインタ
   * @param len コピー元LMBCSの長さ、0終端している場合は-1
   */
  NetPath(const char* data, int len = -1);

  /**
   * @brief パスセットから変換作成するコンストラクタ
   * @param pathSet パスセット
   */
  NetPath(const PathSet& pathSet);

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元
   */
  NetPath(const NetPath& other);

  /**
   * @brief 代入演算子
   * @param other 代入元
   * @return 自身への参照
   */
  NetPath& operator=(const NetPath& other);

  /**
   * @brief デストラクタ
   */
  ~NetPath();

  /**
   * @brief パスセットに変換する
   * @return 変換後のパスセット
   */
  PathSet toPathSet() const;

  /**
   * @brief パス上のファイル情報を収集する
   * @param list ファイル情報の格納先
   * @param fileMask 取得するファイルのマスクデータ
   * @return 結果ステータス
   */
  Status collectFiles(SummaryList& list, WORD fileMask = FILE_DBANY);
};

} // namespace ntlx

#endif // NTLX_NETPATH_H
