﻿#ifndef NTLX_LMBCS_H
#define NTLX_LMBCS_H

#include "ntlx_global.h"
#include "ntlx/status.h"

#include <QByteArray>

namespace ntlx {

/**
 * @brief LMBCSクラス
 */
class NTLXSHARED_EXPORT Lmbcs
    : public QByteArray
{
public:
  /**
   * @brief コンストラクタ
   */
  Lmbcs();

  /**
   * @brief コピー元LMBCSの文字列ポインタと長さによるコンストラクタ
   * @param コピー元LMBCSのポインタ
   * @param len コピー元LMBCSの長さ、0終端している場合は-1
   */
  Lmbcs(const char* s, int len = -1);

  /**
   * @brief ステータス値からエラーメッセージのLMBCSを生成する
   * @param status ステータス値
   */
  Lmbcs(const Status& status);

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元
   */
  Lmbcs(const Lmbcs& other);

  /**
   * @brief 代入演算子
   * @param other 代入元
   * @return 自身への参照
   */
  Lmbcs& operator=(const Lmbcs& other);

  /**
   * @brief デストラクタ
   */
  ~Lmbcs()
  {}

  /**
   * @brief QStringに変換する
   * @return LMBCSから変換されたQString文字列
   */
  QString toQString() const;

  /**
   * @brief QStringからLmbcsに変換する
   * @param qs 変換元のQString
   * @return QStringから変換されたLmbcsオブジェクト
   */
  static Lmbcs fromQString(const QString& qs);
};

/**
 * @brief ヌル文字を改行文字に変換する
 * @param value 変換文字列
 * @return 変換後の文字列への参照
 */
NTLXSHARED_EXPORT QString& nullToCR(QString& value);

/**
 * @brief 改行文字をヌル文字に変換する
 * @param value 変換文字列
 * @return 変換後の文字列への参照
 */
NTLXSHARED_EXPORT QString& CRToNull(QString& value);

} // namespace ntlx

#endif // NTLX_LMBCS_H
