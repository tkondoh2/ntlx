﻿#ifndef NTLX_DBINFO_H
#define NTLX_DBINFO_H

#include "ntlx_global.h"
#include "ntlx/lmbcs.h"

namespace ntlx
{

class NTLXSHARED_EXPORT DbInfo
    : public Lmbcs
{
public:
  /**
   * @brief コンストラクタ
   */
  DbInfo();

  /**
   * @brief コピー元LMBCSの文字列ポインタと長さによるコンストラクタ
   * @param コピー元LMBCSのポインタ
   * @param len コピー元LMBCSの長さ、0終端している場合は-1
   */
  DbInfo(const char* s, int len = -1);

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元
   */
  DbInfo(const DbInfo& other);

  /**
   * @brief 代入演算子
   * @param other 代入元
   * @return 自身への参照
   */
  DbInfo& operator=(const DbInfo& other);

  /**
   * @brief デストラクタ
   */
  ~DbInfo();

  /**
   * @brief データベースのタイトルを取得する
   * @return データベースタイトル
   */
  QString getTitle() const;

  /**
   * @brief データベースカテゴリ名を取得する
   * @return データベースカテゴリ名
   */
  QString getCategories() const;

  /**
   * @brief データベース設計クラス(自身のテンプレート名)を取得する
   * @return データベース設計クラス(自身のテンプレート名)
   */
  QString getClass() const;

  /**
   * @brief データベースクラス(引き継ぎ元テンプレート名)を取得する
   * @return データベースクラス(引き継ぎ元テンプレート名)
   */
  QString getDesignClass() const;

  /**
   * @brief データベース情報を取得する
   * @param what データベース情報の種類(INFOPARSE_XXX)
   * @return データベース情報
   */
  QString getInfo(WORD what) const;
};

} // namespace ntlx

#endif // NTLX_DBINFO_H
