﻿#ifndef NTLX_CD_H
#define NTLX_CD_H

#include <ntlx_global.h>
#include <ntlx/status.h>

namespace ntlx
{

class NTLXSHARED_EXPORT Cd
{
public:
  virtual WORD getSignature() const = 0;
  virtual WORD odsLength() const = 0;
  virtual QString toString() const = 0;
};

} // namespace ntlx

#endif // NTLX_CD_H
