﻿#ifndef NTLX_TIMEDATE_H
#define NTLX_TIMEDATE_H

#include "ntlx_global.h"
#include "ntlx/status.h"

#include <QDateTime>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <misc.h>
#include <ostime.h>

#if defined(NT)
#pragma pack(pop)
#endif

class QDateTime;
class QDate;
class QTime;
class QVariant;

namespace ntlx
{

class Lmbcs;

class NTLXSHARED_EXPORT TimeDate
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  TimeDate();

  /**
   * @brief TIMEDATE構造体を元にしたコンストラクタ
   * @param value TIMEDATE型の日時値
   */
  TimeDate(const TIMEDATE& value);

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元
   */
  TimeDate(const TimeDate& other);

  /**
   * @brief 代入演算子
   * @param other 代入元
   * @return 自身への参照
   */
  TimeDate& operator=(const TimeDate& other);

  /**
   * @brief 現在の日時を取得する
   * @return 現在の日時データ
   */
  static TimeDate current();

  /**
   * @brief 最小値(特殊値)を取得する
   * @return 最小値(特殊値)
   */
  static TimeDate minimum();

  /**
   * @brief 最大値(特殊値)を取得する
   * @return 最大値(特殊値)
   */
  static TimeDate maximum();

  /**
   * @brief ワイルドカード(特殊値)を取得する
   * @return ワイルドカード(特殊値)
   */
  static TimeDate wildcard();

  /**
   * @brief 値をQDateTime型に変換して取得する
   * @return QDateTime型値
   */
  QDateTime toQDateTime() const;

  QDateTime toQtData() const { return toQDateTime(); }

  /**
   * @brief QVariant型に変換する
   * @return 日時のQVariant
   */
  QVariant toVariant() const;

  /**
   * @brief 値をLMBCSテキストに変換する
   * @param intlFormat 国際フォーマット
   * @param textFormat テキストフォーマット
   * @param status 結果ステータス
   * @return Lmbcsオブジェクト
   */
  Lmbcs toLmbcs(
      void* intlFormat = nullptr
      , TFMT* textFormat = nullptr
      , Status* status = nullptr
      ) const;

  /**
   * @brief 値をQStringテキストに変換する
   * @param intlFormat 国際フォーマット
   * @param textFormat テキストフォーマット
   * @param status 結果ステータス
   * @return QStringオブジェクト
   */
  QString toString(
      void* intlFormat = nullptr
      , TFMT* textFormat = nullptr
      , Status* status = nullptr
      ) const;

  /**
   * @brief QDateTime型の値をTimeDateに変換して取得する
   * @param from QDateTime型値
   * @return TimeDate型値
   */
  static TimeDate fromQDateTime(const QDateTime& from);

  static TimeDate fromQtData(const QDateTime& from);

  /**
   * @brief LMBCSテキストからTimeDate型値を取得する
   * @param lmbcs Lmbcsオブジェクト
   * @param intlFormat 国際フォーマット
   * @param textFormat テキストフォーマット
   * @param status 結果ステータス
   * @return TimeDate型値
   */
  static TimeDate fromLmbcs(
      Lmbcs& lmbcs
      , void* intlFormat = nullptr
      , TFMT* textFormat = nullptr
      , Status* status = nullptr
      );

  /**
   * @brief オブジェクトをQVariantから生成する
   * @param var 文字列QVariant
   * @return TimeDateオブジェクト
   */
  static TimeDate fromVariant(const QVariant& var);

  TIMEDATE value_;

protected:
  template<WORD T>
  static TimeDate getConstant()
  {
    TimeDate td;
    TimeConstant(T, &td.value_);
    return td;
  }
};

} // namespace ntlx

#endif // NTLX_TIMEDATE_H
