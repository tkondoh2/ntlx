﻿#ifndef NTLX_CD_OTHER_H
#define NTLX_CD_OTHER_H

#include <ntlx/cd.h>

namespace ntlx
{
namespace cd
{

class NTLXSHARED_EXPORT Other
    : public Cd
{
public:
  Other(char** ppRecord);
  Other(const Other& other);
  Other& operator=(const Other& other);

  virtual WORD getSignature() const;
  virtual WORD odsLength() const;
  virtual QString toString() const;

protected:
  QByteArray record_;
};

} // namespace cd

} // namespace ntlx

#endif // NTLX_CD_OTHER_H
