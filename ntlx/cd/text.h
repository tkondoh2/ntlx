﻿#ifndef NTLX_CD_TEXT_H
#define NTLX_CD_TEXT_H

#include <ntlx/cd/base.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <editods.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace ntlx
{
namespace cd
{

class NTLXSHARED_EXPORT Text
    : public Base<CDTEXT, _CDTEXT, SIG_CD_TEXT>
{
public:
  Text(char** ppRecord);
  Text(const Text& other);
  Text& operator=(const Text& other);

  QString value() const;

  virtual QString toString() const;

private:
  QString value_;
};

} // namespace cd

} // namespace ntlx

#endif // NTLX_CD_TEXT_H
