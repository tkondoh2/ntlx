﻿#ifndef NTLX_CD_BASE_H
#define NTLX_CD_BASE_H

#include <ntlx/cd.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <ods.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace ntlx
{
namespace cd
{

template <typename CDxxx, WORD _CDxxx, WORD SIG_CD_xxx>
class NTLXSHARED_EXPORT Base
    : public Cd
{
public:
  Base(char** ppRecord)
  {
    ODSReadMemory(ppRecord, _CDxxx, &record_, 1);
  }

  Base(const Base& other)
    : record_(other.record_)
  {
  }

  Base& operator=(const Base& other)
  {
    if (this != &other)
      record_ = other.record_;
    return *this;
  }

  virtual WORD getSignature() const
  {
    return SIG_CD_xxx;
  }

  virtual WORD odsLength() const
  {
    return record_.Header.Length + record_.Header.Length % 2;
  }

protected:
  mutable CDxxx record_;
};

} // namespace cd

} // namespace ntlx

#endif // NTLX_CD_BASE_H
