﻿#ifndef NTLX_DISTINGUISHEDNAME_H
#define NTLX_DISTINGUISHEDNAME_H

#include <ntlx/lmbcs.h>

namespace ntlx
{

/**
 * @brief ドメイン名(基準書式名)クラス
 */
class NTLXSHARED_EXPORT DistinguishedName
    : public IStatus
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  DistinguishedName();

  /**
   * @brief コンストラクタ(LMBCS文字列から)
   * @param name 書式化する名前文字列(LMBCS)
   */
  DistinguishedName(const Lmbcs& name);

  /**
   * @brief コンストラクタ(QString文字列から)
   * @param name 書式化する名前文字列(QString)
   */
  DistinguishedName(const QString& name);

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元
   */
  DistinguishedName(const DistinguishedName& other);

  /**
   * @brief 代入演算子
   * @param other 代入元
   * @return 自身への参照
   */
  DistinguishedName& operator=(const DistinguishedName& other);

  /**
   * @brief 文字列が空なら真
   * @return 真/偽
   */
  bool isEmpty() const { return name_.isEmpty(); }

  /**
   * @brief 基準書式をLMBCSで返す
   * @return 基準書式LMBCS
   */
  Lmbcs canonical() const { return name_; }

  /**
   * @brief 省略書式をLMBCSで返す
   * @return 省略書式LMBCS
   */
  Lmbcs abbreviated() const;

  /**
   * @brief 代入演算子
   * @param name 代入元LMBCS
   * @return
   */
  DistinguishedName& operator=(const Lmbcs& name);

  /**
   * @brief 代入演算子
   * @param name 代入元QString
   * @return
   */
  DistinguishedName& operator=(const QString& name);

  /**
   * @brief 等価演算子
   * @param lhs 左辺
   * @param rhs 右辺
   * @return 等価なら真
   */
  friend bool operator==(const DistinguishedName& lhs, const DistinguishedName& rhs);

  /**
   * @brief 不等価演算子
   * @param lhs 左辺
   * @param rhs 右辺
   * @return 不透過なら真
   */
  friend bool operator!=(const DistinguishedName& lhs, const DistinguishedName& rhs);

protected:
  void canonicalize(const Lmbcs& name);

private:
  Lmbcs name_;
};

} // namespace ntlx

#endif // NTLX_DISTINGUISHEDNAME_H
