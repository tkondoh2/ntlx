﻿#ifndef NTLX_ITEM_H
#define NTLX_ITEM_H

#include "ntlx_global.h"
#include "ntlx/status.h"
#include "ntlx/number.h"
#include "ntlx/timedate.h"

#include <QPair>

namespace ntlx
{

/**
 * @brief アイテムクラス
 */
class NTLXSHARED_EXPORT Item
{
public:
  Item();

  Item(
      const QString& name
      , const void* ptr
      , DWORD length
      , WORD flags = 0
      );

  Item(
      const QString& name
      , WORD type
      , const void* ptr
      , DWORD length
      , WORD flags = 0
      );

  Item(
      const QString& name
      , const QByteArray& value
      , WORD flags = 0
      );

  Item(
      const QString& name
      , WORD type
      , const QByteArray& value
      , WORD flags = 0
      );

  Item(const Item& other);

  Item& operator=(const Item& other);

  QString name() const { return name_; }

  WORD type() const { return type_; }

  QByteArray value() const { return value_; }

  WORD flags() const { return flags_; }

  static QString typeName(WORD type);

  static QStringList flagList(WORD flags);

  static QString getText(const char* value, WORD len);

  static QStringList getTextList(const char* value);

  static double getNumber(const char* value);

  static QDateTime getTimeDate(const char* value);

  static QList<double> getNumberList(const char* value);

  static QList<QDateTime> getTimeList(const char* value);

  static QList<QPair<double,double>> getNumberPairList(const char* value);

  static QList<QPair<QDateTime,QDateTime>> getTimePairList(const char* value);

  /**
   * @brief 任意のアイテム型をテキストとして取得する
   * @param type アイテム型
   * @param value アイテムデータ開始ポインタ
   * @param len アイテムデータ長
   * @param ret 結果ステータスへのポインタ
   * @return テキストデータ
   */
  static QString toString(
      WORD type
      , const char* value
      , int len
      , Status* ret = nullptr
      );

  /**
   * @brief 任意のアイテム型をテキストとして取得する
   * @param value アイテム型を含むアイテムデータ開始ポインタ
   * @param len アイテムデータ長
   * @param ret 結果ステータスへのポインタ
   * @return テキストデータ
   */
  static QString toString(
      const char* value
      , WORD len
      , Status* ret = nullptr
      );

protected:
  QString name_;
  WORD type_;
  QByteArray value_;
  WORD flags_;
};

} // namespace ntlx

#endif // NTLX_ITEM_H
