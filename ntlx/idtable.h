﻿#ifndef NTLX_IDTABLE_H
#define NTLX_IDTABLE_H

#include "ntlx_global.h"
#include "ntlx/status.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfdata.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace ntlx
{

class Database;
class Note;

/**
 * @brief IDテーブルクラス
 */
class NTLXSHARED_EXPORT IDTable
    : public IStatus
{
public:

  /**
   * @brief イテレータインナークラス
   */
  class NTLXSHARED_EXPORT iterator
  {
  public:
    /**
     * @brief デフォルトコンストラクタ
     */
    iterator();

    /**
     * @brief NOTEIDを返す演算子
     * @return NOTEID
     */
    NOTEID operator*();

    /**
     * @brief 前置きインクリメント演算子
     * @return 自身への参照
     */
    iterator& operator++();

    /**
     * @brief 後置きインクリメント演算子
     * @return インクリメントする前のイテレータ
     */
    iterator operator++(int);

    /**
     * @brief NOTEIDに対応したNoteオブジェクトを取得する
     * @param db 取得元になるデータベースオブジェクト
     * @return Noteオブジェクト
     */
    Note getNote(Database& db) const;

  protected:
    /**
     * @brief コンストラクタ
     * @param pIdTable IDTableオブジェクトへのポインタ
     * @param noteId NOTEID
     * @param isLast 最後を示しているか
     */
    iterator(IDTable* pIdTable, NOTEID noteId, bool isLast = false);

    IDTable* idTable_;
    NOTEID id_;
    bool isLast_;

    /**
     * @brief 等値演算子
     * @param lhs 左辺値
     * @param rhs 右辺値
     * @return ブール値
     */
    friend NTLXSHARED_EXPORT bool operator==(
        const iterator& lhs
        , const iterator& rhs
        );

    /**
     * @brief 不等値演算子
     * @param lhs 左辺値
     * @param rhs 右辺値
     * @return ブール値
     */
    friend NTLXSHARED_EXPORT bool operator!=(
        const iterator& lhs
        , const iterator& rhs
        );

    friend class IDTable;
  };

  /**
   * @brief コンストラクタ
   */
  IDTable();

  /**
   * @brief デストラクタ
   */
  virtual ~IDTable();

  /**
   * @brief 最初のイテレータを返す
   * @return 最初のイテレータ
   */
  iterator begin();

  /**
   * @brief 最後のイテレータを返す
   * @return 最後のイテレータ
   */
  iterator end();

  /**
   * @brief NOTEIDを挿入する
   * @param id NOTEID
   * @return 重複していなければ真
   */
  bool insert(NOTEID id);

private:
  DHANDLE handle_;

  friend class iterator;
};

} // namespace ntlx

#endif // IDTABLE_H
