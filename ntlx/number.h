﻿#ifndef NTLX_NUMBER_H
#define NTLX_NUMBER_H

#include "ntlx_global.h"
#include "ntlx/status.h"

class QVariant;

namespace ntlx
{

/**
 * @brief 数値ラッパークラス
 */
class NTLXSHARED_EXPORT Number
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  Number();

  /**
   * @brief コンストラクタ(NUMBER型から)
   * @param value NUMBER値
   */
  Number(const NUMBER& value);

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元
   */
  Number(const Number& other);

  /**
   * @brief 代入演算子
   * @param other 代入元
   * @return 自身への参照
   */
  Number& operator=(const Number& other);

  NUMBER toQtData() const { return value_; }

  /**
   * @brief 文字列に変換する
   * @return 数値文字列
   */
  QString toString() const;

  /**
   * @brief QVariantオブジェクトに変換する
   * @return QVariant値
   */
  QVariant toVariant() const;

  static Number fromQtData(NUMBER from);
  /**
   * @brief QVariant型からNumber型に変換する
   * @param var QVariant値
   * @return Numberオブジェクト
   */
  static Number fromVariant(const QVariant& var);

private:
  NUMBER value_;
};

} // namespace ntlx

#endif // NTLX_NUMBER_H
