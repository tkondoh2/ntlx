﻿#ifndef NTLX_STATUS_H
#define NTLX_STATUS_H

#include "ntlx_global.h"
#include <QString>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace ntlx {

/**
 * @brief ステータスクラス
 */
class NTLXSHARED_EXPORT Status
{
public:

  /**
   * @brief コンストラクタ
   */
  Status()
  {}

  /**
   * @brief コンストラクタ
   * @param value ステータス値
   */
  Status(STATUS value)
    : value_(value)
  {}

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元
   */
  Status(const Status& other)
    : value_(other.value_)
  {}

  /**
   * @brief 代入演算子
   * @param other 代入元
   * @return 自身への参照
   */
  Status& operator=(const Status& other)
  {
    if (this == &other) return *this;
    value_ = other.value_;
    return *this;
  }

  /**
   * @brief キャスト演算子
   */
  operator STATUS() const { return value_; }

  /**
   * @brief エラー値
   * @return 上位2ピットをマスクしたエラー値
   */
  STATUS error() const { return ERR(value_); }

  /**
   * @brief エラーがないと真を返す
   * @return エラーがなければ真
   */
  bool success() const { return error() == NOERROR; }

  /**
   * @brief エラーがあると真を返す
   * @return エラーがあれば真
   */
  bool failure() const { return !success(); }

  /**
   * @brief すでに表示されていれば真を返す
   * @return 表示済であれば真
   */
  bool hasDisplayed() const { return ((value_ & STS_DISPLAYED) != 0); }

  /**
   * @brief リモート(Dominoサーバ)起因のエラーであれば真を返す
   * @return リモート起因なら真
   */
  bool isRemote() const { return ((value_ & STS_REMOTE) != 0); }

  /**
   * @brief メッセージ文字列を返す
   * @return 対応するメッセージ文字列
   */
  QString toMessage() const;

private:
  STATUS value_;
};

/**
 * @brief IStatusインターフェースクラス
 */
class IStatus
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  IStatus()
    : lastStatus_()
  {}

  /**
   * @brief 最後の結果ステータスを返す
   * @return 最後の結果ステータス
   */
  Status lastStatus() const { return lastStatus_; }

  /**
   * @brief 最後の結果ステータスをクリアする
   */
  void clearStatus() { lastStatus_ = NOERROR; }

protected:
  mutable Status lastStatus_;
};

/**
 * @brief API初期化関数
 * @param argc オプション数
 * @param argv オプション文字列へのポインタ
 * @return 初期化の結果ステータス
 */
NTLXSHARED_EXPORT Status initEx(int argc, char** argv);

/**
 * @brief API終了関数
 */
NTLXSHARED_EXPORT void term();

/**
 * @brief サーバリストを取得する
 * @param list 格納先となるQStringのリストオブジェクトへの参照
 * @param port 検索するポート名
 * @return 処理ステータス
 */
NTLXSHARED_EXPORT Status getServers(
    QList<QString>& list
    , const QString& port = ""
    );

}

#endif // NTLX_STATUS_H
