﻿#ifndef NTLX_PATHSET_H
#define NTLX_PATHSET_H

#include "ntlx_global.h"
#include <QString>

namespace ntlx
{

class NetPath;

/**
 * @brief パスセットクラス
 */
class NTLXSHARED_EXPORT PathSet
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  PathSet();

  /**
   * @brief パラメータによるコンストラクタ
   * @param path パス
   * @param server サーバ名(省略時はローカル)
   * @param port ポート名(省略時はデフォルトポート)
   */
  PathSet(
      const QString& path
      , const QString& server = QString()
      , const QString& port = QString()
      );

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元
   */
  PathSet(const PathSet& other);

  /**
   * @brief 代入演算子
   * @param other 代入元
   * @return 自身への参照
   */
  PathSet& operator=(const PathSet& other);

  /**
   * デストラクタ
   */
  ~PathSet();

  /**
   * @brief パスを返す
   * @return パス
   */
  QString path() const { return path_; }

  /**
   * @brief サーバを返す
   * @return サーバ
   */
  QString server() const { return server_; }

  /**
   * @brief ポートを返す
   * @return ポート
   */
  QString port() const { return port_; }

  /**
   * @brief ネットパスに変換する
   * @return ネットパス
   */
  NetPath toNetPath() const;

private:
  QString path_;
  QString server_;
  QString port_;
};

} // namespace ntlx

#endif // NTLX_PATHSET_H
