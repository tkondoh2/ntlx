﻿#ifndef NTLX_SUMMARYMAP_H
#define NTLX_SUMMARYMAP_H

#include <ntlx/status.h>
#include <QMap>
#include <QList>
#include <QVariant>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfdata.h>
#include <nsfsearc.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace ntlx
{

/**
 * @brief 名前付きサマリーバッファ用データクラス
 */
class NTLXSHARED_EXPORT SummaryMap
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  SummaryMap();

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元
   */
  SummaryMap(const SummaryMap& other);

  /**
   * @brief 代入演算子
   * @param other 代入元
   * @return 自身への参照
   */
  SummaryMap& operator=(const SummaryMap& other);

  /**
   * @brief 検索マッチ情報
   * @return SEARCH_MATCH型データへの参照
   */
  const SEARCH_MATCH& match() const { return match_; }

  QMap<QString, QVariant>& map() { return map_; }

  /**
   * @brief load NSFSearch関数のコールバック情報を取り込む
   * @param pMatch 検索マッチ情報
   * @param pItemTable サマリーバッファへのポインタ
   */
  void load(const SEARCH_MATCH* const pMatch, ITEM_TABLE* pItemTable);

  /**
   * @brief 型タイプに合わせてデータを取得する
   * @param type 型タイプ
   * @param ptr データへのポインタ
   * @param len データ長
   * @return データのQVariantオブジェクト
   */
  static QVariant getValue(WORD type, char* ptr, USHORT len);

private:
  SEARCH_MATCH match_;
  QMap<QString, QVariant> map_;
};

/**
 * @brief サマリーバッファのリストクラス
 */
typedef QList<SummaryMap> SummaryList;

} // namespace ntlx

#endif // NTLX_SUMMARYMAP_H
