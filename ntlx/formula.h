﻿#ifndef NTLX_FORMULA_H
#define NTLX_FORMULA_H

#include "ntlx_global.h"
#include "ntlx/status.h"
#include "ntlx/note.h"

namespace ntlx
{

/**
 * @brief 式クラス
 */
class NTLXSHARED_EXPORT Formula
    : public IStatus
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  Formula();

  /**
   * @brief コンストラクタ
   * @param bin コンパイル済み@式へのポインタ
   * @param size コンパイル済み@式の長さ
   */
  Formula(const char* bin, WORD size);

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元Formula
   */
  Formula(const Formula& other);

  /**
   * @brief 代入演算子
   * @param other 代入元Formula
   * @return 自身への参照
   */
  Formula& operator=(const Formula& other);

  /**
   * @brief デストラクタ
   */
  virtual ~Formula();

  /**
   * @brief コンパイル済み@式のサイズを取得する
   * @return コンパイル済み@式のサイズ
   */
  WORD odsLength() const;

  /**
   * @brief 式ハンドルをロックする
   * @return コンパイル済み@式へのポインタ
   */
  const char* lock() const;

  /**
   * @brief コンパイル済み@式を割り当てる
   * @param bin コンパイル済み@式
   * @param size コンパイル済み@式の長さ
   */
  void assign(const char* bin, WORD size);

  /**
   * @brief 式を評価する
   * @param note 評価元になる文書オブジェクト
   * @return 評価した式の結果テキスト
   */
  QString evaluate(Note& note);

protected:

  /**
   * @brief 計算を開始する
   * @return 計算ハンドルを取得できれば真
   */
  bool startCompute();

  /**
   * @brief 計算を終了する
   */
  void stopCompute() const;

  /**
   * @brief 式ハンドルをアンロックする
   */
  void unlock() const;

  /**
   * @brief 式を閉じる
   */
  void close() const;

  mutable FORMULAHANDLE handle_;
  mutable char* bin_;
  mutable HCOMPUTE hCompute_;
};

} // namespace ntlx

#endif // NTLX_FORMULA_H
