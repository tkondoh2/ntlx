﻿#include "ntlx/item.h"
#include "ntlx/lmbcs.h"
#include <QString>
#include <QStringList>
#include <QDateTime>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfdata.h>
#include <nsfnote.h>
#include <textlist.h>
#include <misc.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace ntlx
{

Item::Item()
  : name_()
  , type_(TYPE_INVALID_OR_UNKNOWN)
  , value_()
  , flags_(0)
{
}

Item::Item(
    const QString& name
    , const void* ptr
    , DWORD length
    , WORD flags
    )
  : name_(name)
  , type_(*(WORD*)ptr)
  , value_(((char*)ptr) + sizeof(WORD), length - sizeof(WORD))
  , flags_(flags)
{
}

Item::Item(
    const QString& name
    , WORD type
    , const void* ptr
    , DWORD length
    , WORD flags
    )
  : name_(name)
  , type_(type)
  , value_((char*)ptr, length)
  , flags_(flags)
{
}

Item::Item(
    const QString& name
    , const QByteArray& value
    , WORD flags
    )
  : name_(name)
  , type_(*(WORD*)value.constData())
  , value_(value.constData() + sizeof(WORD), value.length() - sizeof(WORD))
  , flags_(flags)
{
}

Item::Item(
    const QString& name
    , WORD type
    , const QByteArray& value
    , WORD flags
    )
  : name_(name)
  , type_(type)
  , value_(value)
  , flags_(flags)
{
}

Item::Item(const Item &other)
  : name_(other.name_)
  , type_(other.type_)
  , value_(other.value_)
  , flags_(other.flags_)
{
}

Item& Item::operator=(const Item &other)
{
  if (this == &other) return *this;

  name_ = other.name_;
  type_ = other.type_;
  value_ = other.value_;
  flags_ = other.flags_;
  return *this;
}

QString Item::typeName(WORD type)
{
  QString className, typeName_;

  switch (type & CLASS_MASK)
  {
  case CLASS_NOCOMPUTE: className = "NOCOMPUTE"; break;
  case CLASS_ERROR: className = "ERROR"; break;
  case CLASS_UNAVAILABLE: className = "UNAVAILABLE"; break;
  case CLASS_NUMBER: className = "NUMBER"; break;
  case CLASS_TIME: className = "TIME"; break;
  case CLASS_TEXT: className = "TEXT"; break;
  case CLASS_FORMULA: className = "FORMULA"; break;
  case CLASS_USERID: className = "USERID"; break;
  }

  switch (type)
  {
  case TYPE_ERROR: typeName_ = "ERROR"; break;
  case TYPE_UNAVAILABLE: typeName_ = "UNAVAILABLE"; break;
  case TYPE_TEXT: typeName_ = "TEXT"; break;
  case TYPE_TEXT_LIST: typeName_ = "TEXT_LIST"; break;
  case TYPE_NUMBER: typeName_ = "NUMBER"; break;
  case TYPE_NUMBER_RANGE: typeName_ = "NUMBER_RANGE"; break;
  case TYPE_TIME: typeName_ = "TIME"; break;
  case TYPE_TIME_RANGE: typeName_ = "TIME_RANGE"; break;
  case TYPE_FORMULA: typeName_ = "FORMULA"; break;
  case TYPE_USERID: typeName_ = "USERID"; break;
  case TYPE_INVALID_OR_UNKNOWN: typeName_ = "INVALID_OR_UNKNOWN"; break;
  case TYPE_COMPOSITE: typeName_ = "COMPOSITE"; break;
  case TYPE_COLLATION: typeName_ = "COLLATION"; break;
  case TYPE_OBJECT: typeName_ = "OBJECT"; break;
  case TYPE_NOTEREF_LIST: typeName_ = "NOTEREF_LIST"; break;
  case TYPE_VIEW_FORMAT: typeName_ = "VIEW_FORMAT"; break;
  case TYPE_ICON: typeName_ = "ICON"; break;
  case TYPE_NOTELINK_LIST: typeName_ = "NOTELINK_LIST"; break;
  case TYPE_SIGNATURE: typeName_ = "SIGNATURE"; break;
  case TYPE_SEAL: typeName_ = "SEAL"; break;
  case TYPE_SEALDATA: typeName_ = "SEALDATA"; break;
  case TYPE_SEAL_LIST: typeName_ = "SEAL_LIST"; break;
  case TYPE_HIGHLIGHTS: typeName_ = "HIGHLIGHTS"; break;
  case TYPE_WORKSHEET_DATA: typeName_ = "WORKSHEET_DATA"; break;
  case TYPE_USERDATA: typeName_ = "USERDATA"; break;
  case TYPE_QUERY: typeName_ = "QUERY"; break;
  case TYPE_ACTION: typeName_ = "ACTION"; break;
  case TYPE_ASSISTANT_INFO: typeName_ = "ASSISTANT_INFO"; break;
  case TYPE_VIEWMAP_DATASET: typeName_ = "VIEWMAP_DATASET"; break;
  case TYPE_VIEWMAP_LAYOUT: typeName_ = "VIEWMAP_LAYOUT"; break;
  case TYPE_LSOBJECT: typeName_ = "LSOBJECT"; break;
  case TYPE_HTML: typeName_ = "HTML"; break;
  case TYPE_SCHED_LIST: typeName_ = "SCHED_LIST"; break;
  case TYPE_CALENDAR_FORMAT: typeName_ = "CALENDAR_FORMAT"; break;
  case TYPE_MIME_PART: typeName_ = "MIME_PART"; break;
  case TYPE_RFC822_TEXT: typeName_ = "RFC822_TEXT"; break;
  case TYPE_SEAL2: typeName_ = "SEAL2"; break;
  }

  return QString("%1/%2").arg(typeName_).arg(className);
}

QStringList Item::flagList(WORD flags)
{
  QStringList list;
  if (flags & ITEM_SIGN) list << "SIGN";
  if (flags & ITEM_SEAL) list << "SEAL";
  if (flags & ITEM_SUMMARY) list << "SUMMARY";
  if (flags & ITEM_READWRITERS) list << "READWRITERS";
  if (flags & ITEM_NAMES) list << "NAMES";
  if (flags & ITEM_PLACEHOLDER) list << "PLACEHOLDER";
  if (flags & ITEM_PROTECTED) list << "PROTECTED";
  if (flags & ITEM_READERS) list << "READERS";
  if (flags & ITEM_UNCHANGED) list << "UNCHANGED";
  return list;
}

QString Item::getText(const char *value, WORD len)
{
  Lmbcs lmbcs(value, len);
  return lmbcs.toQString();
}

QStringList Item::getTextList(const char *value)
{
  LIST* list = (LIST*)value;
  WORD* pWord = (WORD*)(value + sizeof(LIST));
  QStringList values;
  for (WORD index = 0; index < list->ListEntries; ++index)
  {
    char* pText;
    WORD wTextLen;
    Status result = ListGetText(list, 0, index, &pText, &wTextLen);
    if (result.failure()) continue;
    Lmbcs lmbcs(pText, *pWord++);
    values.append(lmbcs.toQString());
  }
  return values;
}

double Item::getNumber(const char *value)
{
  return *(NUMBER*)value;
}

QDateTime Item::getTimeDate(const char *value)
{
  TimeDate td;
  td = *(TIMEDATE*)value;
  return td.toQDateTime();
}

template<class T, typename S, typename D>
QList<D> getSingleListValue(const char* ptr)
{
  RANGE* pRange = (RANGE*)ptr;
  S* pSingle = (S*)(pRange + 1);
  QList<D> list;
  for (USHORT i = 0; i < pRange->ListEntries; ++i, ++pSingle)
  {
    T item(*pSingle);
    list.append(item.toQtData());
  }
  return list;
}

QList<double> Item::getNumberList(const char *value)
{
  return getSingleListValue<Number, NUMBER, NUMBER>(value);
}

QList<QDateTime> Item::getTimeList(const char *value)
{
  return getSingleListValue<TimeDate, TIMEDATE, QDateTime>(value);
}

template<class T, typename S, typename P, typename D>
QList<QPair<D,D>> getPairListValue(const char* ptr)
{
  RANGE* pRange = (RANGE*)ptr;
  S* pSingle = (S*)(pRange + 1);
  P* pPair = (P*)(pSingle + pRange->ListEntries);

  QList<QPair<D,D>> pairList;
  for (USHORT i = 0; i < pRange->RangeEntries; ++i, ++pPair)
  {
    T lower(pPair->Lower);
    T upper(pPair->Upper);
    QPair<D, D> pair(lower.toQtData(), upper.toQtData());
    pairList.append(pair);
  }
  return pairList;
}

QList<QPair<double,double>> Item::getNumberPairList(const char *value)
{
  return getPairListValue<Number, NUMBER, NUMBER_PAIR, double>(value);
}

QList<QPair<QDateTime,QDateTime>> Item::getTimePairList(const char *value)
{
  return getPairListValue<TimeDate, TIMEDATE, TIMEDATE_PAIR, QDateTime>(value);
}

template<typename T, class R>
QStringList stringifyList(const QList<T>& rawList)
{
  QStringList list;
  for (auto i = rawList.constBegin(); i != rawList.constEnd(); ++i)
  {
    R item = R::fromQtData(*i);
    list.append(item.toString());
  }
  return list;
}

template<typename T, class R>
QStringList joinPairList(const QList<QPair<T,T>>& pairList
                         , const QString& sep = " - ")
{
  QStringList list;
  for (auto i = pairList.constBegin(); i != pairList.constEnd(); ++i)
  {
    R lower = R::fromQtData((*i).first);
    R upper = R::fromQtData((*i).second);
    list.append(lower.toString() + sep + upper.toString());
  }
  return list;
}

QString Item::toString(
    WORD type
    , const char* value
    , int len
    , Status* ret
    )
{
  if (ret != nullptr) *ret = NOERROR;

  switch (type)
  {
  case TYPE_TEXT:
    return getText(value, len);

  case TYPE_TEXT_LIST:
    return getTextList(value).join(",");

  case TYPE_NUMBER:
    return QString::number(getNumber(value));

  case TYPE_TIME:
  {
    QDateTime td(getTimeDate(value));
    return td.toString("yyyy/MM/dd HH:mm:ss");
  }

  case TYPE_NUMBER_RANGE:
  {
    QList<double> singleList = getNumberList(value);
    QList<QPair<double,double>> pairList = getNumberPairList(value);
    QList<QString> strList = stringifyList<double, Number>(singleList);
    QList<QString> joinedPairList = joinPairList<double, Number>(pairList);
    return QString("{%1}{%2}")
        .arg(strList.join(","))
        .arg(joinedPairList.join(","))
        ;
  }

  case TYPE_TIME_RANGE:
  {
    QList<QDateTime> singleList = getTimeList(value);
    QList<QPair<QDateTime,QDateTime>> pairList = getTimePairList(value);
    QList<QString> strList = stringifyList<QDateTime, TimeDate>(singleList);
    QList<QString> joinedPairList = joinPairList<QDateTime, TimeDate>(pairList);
    return QString("{%1}{%2}")
        .arg(strList.join(","))
        .arg(joinedPairList.join(","))
        ;
  }

  }

  QStringList bins;
  for (int i = 0; i < len; ++i)
  {
    bins.append(QString("%1")
                .arg(static_cast<uint>(value[i]), 2, 16, QChar('0'))
                .right(2)
                );
  }
  return bins.join(" ");
}

QString Item::toString(
    const char* value
    , WORD len
    , Status* ret
    )
{
  if (len == 0) return QString();
  return toString(
        *reinterpret_cast<const WORD*>(value)
        , value + sizeof(WORD)
        , len - sizeof(WORD)
        , ret
        );
}

} // namespace ntlx
