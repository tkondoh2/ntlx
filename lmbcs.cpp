﻿#include "ntlx/lmbcs.h"

#include <QString>
#include <QtDebug>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <osmisc.h>
#include <nls.h>
#include <misc.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace ntlx {

const WORD UNICODE_BYTE = (sizeof(ushort) / sizeof(char));
const WORD LMBCS_DELTA = (1024 * 31);
const int UNICODE_DELTA = (1024 * 42 / UNICODE_BYTE);

Lmbcs::Lmbcs()
  : QByteArray()
{
}

Lmbcs::Lmbcs(const char *s, int len)
  : QByteArray(s, len)
{
}

Lmbcs::Lmbcs(const Status& status)
  : QByteArray()
{
  char buffer[MAXSPRINTF];
  WORD lmbcsLen = OSLoadString(0, status.error(), buffer, MAXSPRINTF);
  *this = Lmbcs(buffer, lmbcsLen);
}

Lmbcs::Lmbcs(const Lmbcs& other)
  : QByteArray(other)
{
}

Lmbcs& Lmbcs::operator=(const Lmbcs& other)
{
  if (this == &other) return *this;
  QByteArray::operator=(other);
  return *this;
}

QString Lmbcs::toQString() const
{
  // 変換後の文字列をセットする変数
  QString result;
  char buffer[MAXWORD] = "";

  // 現在の文字列のポインタとサイズ(バイト数)を計算する
  BYTE* ptr = (BYTE*)constData();
  int restSize = size();

  // 1バイトでかつ制御文字の場合は\xXX形式で表現して返す
  if (restSize == 1)
  {
    if (*ptr < 0x20)
      return QString("\\x%1").arg(*ptr, 2, 16, QChar('0'));
  }

  // キャラクタセットを設定する
  NLS_PINFO pLmbcsInfo = OSGetLMBCSCLS();
  NLS_PINFO pUnicodeInfo;
  NLS_load_charset(NLS_CS_UNICODE, &pUnicodeInfo);

  while (restSize > 0)
  {
    // 想定する境界値を計算する
    WORD delta = restSize < (int)LMBCS_DELTA ? (WORD)restSize : LMBCS_DELTA;

    // 境界値から文字数を割り出す
    WORD chars = 0;
    NLS_string_chars(ptr, delta, &chars, pLmbcsInfo);

    // 文字数から実際のバイト数を割り出す
    WORD bytes = 0;
    NLS_string_bytes(ptr, chars, &bytes, pLmbcsInfo);

    if (bytes < 1)
      bytes = restSize;

    // LMBCSからUNICODEに変換する
    WORD retLen = MAXWORD;
    NLS_STATUS status = NLS_translate(
          ptr, bytes
          , (BYTE*)buffer, &retLen
          , NLS_NONULLTERMINATE | NLS_SOURCEISLMBCS | NLS_TARGETISUNICODE
          , pUnicodeInfo
          );
    qDebug() << *ptr << restSize << delta << chars << bytes << retLen;
    Q_ASSERT(status == NLS_SUCCESS);

    // UNICODE配列からQStringを作成してストリームに追加する
    result += QString::fromUtf16(reinterpret_cast<ushort*>(buffer)
                                 , (int)retLen / UNICODE_BYTE
                                 );

    // ポインタを進め、残りサイズを減らす
    ptr += (int)bytes;
    restSize -= bytes;
  }

  // ロードしたUNICODEキャラクタセットをアンロードする
  NLS_unload_charset(pUnicodeInfo);

  // ストリームを介して作成したQString文字列を返す
  return result;
}

Lmbcs Lmbcs::fromQString(const QString &qs)
{
  // 変換後の文字列をセットする変数
  QByteArray result;
  char buffer[MAXWORD] = "";

  // キャラクタセットを設定する
  NLS_PINFO pLmbcsInfo = OSGetLMBCSCLS();

  // 開始文字位置とサイズ(文字数)を計算する。
  int index = 0, restSize = qs.size();

  while (restSize > 0)
  {
    // 定義済みのUNICODE処理文字数と比較して変換する文字数を決める
    int chars = restSize < (int)UNICODE_DELTA ? restSize : (int)UNICODE_DELTA;

    // 開始位置と変換文字数から文字列を抜き出す
    QString input = qs.mid(index, chars);

    // 抜き出した文字列をUTF-16の配列に変換する
    const ushort* unicode = input.utf16();

    // 変換するバイト数を計算する
    WORD unicodeLen = (WORD)input.size() * UNICODE_BYTE;

    // UNICODEからLMBCSに変換する
    WORD retLen = MAXWORD;
    NLS_STATUS status = NLS_translate(
          reinterpret_cast<BYTE*>(const_cast<ushort*>(unicode)), unicodeLen
          , (BYTE*)buffer, &retLen
          , NLS_NONULLTERMINATE | NLS_SOURCEISUNICODE | NLS_TARGETISLMBCS
          , pLmbcsInfo
          );
    Q_ASSERT(status == NLS_SUCCESS);

    // LMBCS配列をQByteArrayに追加する
    result.append(buffer, (int)retLen);

    // 開始文字位置を進め、残りサイズを減らす
    index += chars;
    restSize -= chars;
  }

  // ストリームを介して作成したバイト列をLmbcsオブジェクトにして返す
  return Lmbcs(result.constData(), result.size());
}

QString& nullToCR(QString& value)
{
  value.replace(QChar('\0'), QChar('\n'));
  return value;
}

QString& CRToNull(QString& value)
{
  value.replace(QChar('\n'), QChar('\0'));
  return value;
}

} // namespace ntlx
