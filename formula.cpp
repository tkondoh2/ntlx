﻿#include "ntlx/formula.h"
#include "ntlx/item.h"
#include <QString>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <osmem.h>
#include <nsfsearc.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace ntlx
{

Formula::Formula()
  : IStatus()
  , handle_(NULLHANDLE)
  , bin_(nullptr)
  , hCompute_(NULLHANDLE)
{
}

Formula::Formula(const char *bin, WORD size)
    : IStatus()
    , handle_(NULLHANDLE)
    , bin_(nullptr)
    , hCompute_(NULLHANDLE)
{
  assign(bin, size);
}

Formula::Formula(const Formula &other)
    : IStatus()
    , handle_(NULLHANDLE)
    , bin_(nullptr)
    , hCompute_(NULLHANDLE)
{
  assign(other.lock(), other.odsLength());
}

Formula& Formula::operator=(const Formula& other)
{
  if (this != &other) return *this;
  assign(other.lock(), other.odsLength());
  return *this;
}

Formula::~Formula()
{
  close();
}

WORD Formula::odsLength() const
{
  if (handle_ == NULLHANDLE) return 0;

  WORD len;
  lastStatus_ = NSFFormulaGetSize(handle_, &len);
  return len;
}

const char* Formula::lock() const
{
  if (handle_ == NULLHANDLE)
  {
    Q_ASSERT(bin_ == nullptr);
    return nullptr;
  }

  if (bin_ == nullptr)
    bin_ = OSLock(char, handle_);

  return bin_;
}

void Formula::assign(const char *bin, WORD size)
{
  close();
  if (size == 0) return;

  lastStatus_ = OSMemAlloc(0, static_cast<DWORD>(size), &handle_);
  if (lastStatus_.failure())
    return;

  Q_ASSERT(handle_ != NULLHANDLE);
  bin_ = OSLock(char, handle_);
  memcpy(bin_, bin, size);
}

QString Formula::evaluate(Note &note)
{
  if (lock() == nullptr) return QString();

  if (!startCompute())
    return lastStatus_.toMessage();

  NOTEHANDLE hNote = NULLHANDLE;
  Q_ASSERT(note.db_ != nullptr && note.id_ != 0);

  if (note.open().lastStatus().failure())
    return QString();

  hNote = note.handle_;

  DHANDLE hResult = NULLHANDLE;
  WORD len = 0;
  lastStatus_ = NSFComputeEvaluate(
        hCompute_
        , hNote
        , &hResult
        , &len
        , 0
        , 0
        , 0
        );

  if (lastStatus_.success())
  {
    Q_ASSERT(hResult != NULLHANDLE);
    char* pResult = OSLock(char, hResult);
    QString result = Item::toString(pResult, len);
    OSUnlock(hResult);
    OSMemFree(hResult);
    return result;
  }
  else
    return lastStatus_.toMessage();
}

bool Formula::startCompute()
{
  if (hCompute_ == NULLHANDLE)
  {
    lastStatus_ = NSFComputeStart(0, bin_, &hCompute_);
    return lastStatus_.success();
  }
  return true;
}

void Formula::stopCompute() const
{
  if (hCompute_ != NULLHANDLE)
  {
    NSFComputeStop(hCompute_);
    hCompute_ = NULLHANDLE;
  }
}

void Formula::unlock() const
{
  if (bin_ != nullptr)
  {
    Q_ASSERT(handle_ != NULLHANDLE);
    OSUnlock(handle_);
    bin_ = nullptr;
  }
}

void Formula::close() const
{
  stopCompute();
  unlock();
  if (handle_ != NULLHANDLE)
  {
    OSMemFree(handle_);
    handle_ = NULLHANDLE;
  }
}

} // namespace ntlx
