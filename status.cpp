﻿#include <QList>
#include "ntlx/status.h"
#include "ntlx/lmbcs.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <ns.h>
#include <osmem.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace ntlx
{

QString Status::toMessage() const
{
  Lmbcs lmbcs(value_);
  return lmbcs.toQString();
}

Status initEx(int argc, char **argv)
{
  return NotesInitExtended(argc, argv);
}

void term()
{
  return NotesTerm();
}

Status getServers(QList<QString>& list, const QString& port)
{
  HANDLE handle = NULLHANDLE;
  Status result = NSGetServerList(
        port.isEmpty() ? 0 : Lmbcs::fromQString(port).data()
        , &handle
        );
  if (result.failure())
    return result;
  Q_ASSERT(handle);

  WORD* pLen = (WORD*)OSLockObject(handle);
  WORD count = *pLen++;
  char* pValue = (char*)(pLen + count);
  for (WORD i = 0; i < count; ++i)
  {
    Lmbcs value(pValue, *pLen);
    list.append(value.toQString());
    pValue += *pLen++;
  }

  OSUnlockObject(handle);
  OSMemFree(handle);
  return result;
}

} // namespace ntlx
