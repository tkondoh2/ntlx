#-------------------------------------------------
#
# Project created by QtCreator 2017-03-12T14:23:47
#
#-------------------------------------------------

QT       -= gui

TARGET = ntlx
TEMPLATE = lib

DEFINES += NTLX_LIBRARY

VERSION = 0.1.5

SOURCES += lmbcs.cpp \
    database.cpp \
    status.cpp \
    netpath.cpp \
    pathset.cpp \
    dbinfo.cpp \
    idtable.cpp \
    note.cpp \
    formula.cpp \
    item.cpp \
    timedate.cpp \
    number.cpp \
    summarymap.cpp \
    distinguishedname.cpp \
    richtextitem.cpp \
    cd.cpp \
    cd/other.cpp \
    cd/text.cpp \
    cd/base.cpp

HEADERS +=\
        ntlx_global.h \
    ntlx/status.h \
    ntlx/pathset.h \
    ntlx/netpath.h \
    ntlx/lmbcs.h \
    ntlx/idtable.h \
    ntlx/dbinfo.h \
    ntlx/database.h \
    ntlx/note.h \
    ntlx/formula.h \
    ntlx/item.h \
    ntlx/timedate.h \
    ntlx/summarymap.h \
    ntlx/number.h \
    ntlx/distinguishedname.h \
    ntlx/richtextitem.h \
    ntlx/ivariant.h \
    ntlx/cd.h \
    ntlx/cd/other.h \
    ntlx/cd/text.h \
    ntlx/cd/base.h

win32 {
    DEFINES += W32 NT
}
else:macx {
    # MACのDWORDをunsigned int(4バイト)にする
    # DEFINES += MAC
    DEFINES += MAC LONGIS64BIT
}
else:unix {
    DEFINES += UNIX LINUX W32
    QMAKE_CXXFLAGS += -std=c++0x
    target.path = /usr/lib
    INSTALLS += target
}

LIBS += -lnotes

DISTFILES += \
    .gitignore
