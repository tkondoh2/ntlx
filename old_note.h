﻿#ifndef NTLX_NOTE_H
#define NTLX_NOTE_H

#include "ntlx_global.h"
#include "status.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfnote.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace ntlx
{

class Database;

/**
 * @brief ノート(文書)クラス
 */
class NTLXSHARED_EXPORT Note
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  Note();

  /**
   * @brief データベースとNOTEIDを元にしたコンストラクタ
   * @param db データベースへの参照
   * @param nodeId NOTEID
   */
  Note(Database& db, NOTEID nodeId = 0);

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元
   */
  Note(const Note& other);

  /**
   * @brief 代入演算子
   * @param other 代入元
   * @return 自身への参照
   */
  Note& operator=(const Note& other);

  /**
   * @brief デストラクタ
   */
  virtual ~Note();

  /**
   * @brief NOTEIDを設定、変更する
   * @param noteId NOTEID
   */
  void assign(NOTEID noteId);

  /**
   * @brief NOTEIDとデータベースを変更する
   * @param noteId NOTEID
   * @param pDb データベースへのポインタ
   */
  void assign(NOTEID noteId, Database* pDb);

  /**
   * @brief 文書を開く
   * @param オープンフラグ
   * @return 結果ステータス
   */
  virtual Status open(WORD openFlags = 0) const;

  /**
   * @brief 文書を閉じる
   * @return 結果ステータス
   */
  Status close() const;

  /**
   * @brief 文書の変更を更新する
   * @param updateFlags 更新フラグ
   * @return 結果ステータス
   */
  Status update(WORD updateFlags = 0);

  /**
   * @brief NOTEIDを返す
   * @return NOTEID
   */
  NOTEID noteId() const { return noteId_; }

  /**
   * @brief 文書ハンドルへのキャスト演算子
   */
  operator NOTEHANDLE() const
  {
    open();
    return handle_;
  }

  /**
   * @brief 指定の文書情報を取得する
   * @param _note_xxx 文書情報の種類
   * @param pData 情報の格納先
   * @param status 結果ステータス
   */
  void getInfo(WORD _note_xxx, void* pData, Status* status = nullptr);

  /**
   * @brief アイテムの存在を確認する
   * @param itemName アイテム名
   * @param status 結果ステータス
   * @return 存在すれば真
   */
  bool hasItem(const QString& itemName, Status* status = nullptr);

  /**
   * @brief 文書にテキストを書き込む
   * @param itemName アイテム名
   * @param text テキスト
   * @param isSummary サマリーフラグを立てるか
   * @return 結果ステータス
   */
  Status setText(const QString& itemName
                 , const QString& text
                 , bool isSummary = true
                 );

  /**
   * @brief 文書にテキストを書き込む(ポインタ直接指定)
   * @param itemName アイテム名
   * @param text LMBCS(char*)
   * @param len LMBCSの長さ
   * @param isSummary サマリーフラグを立てるか
   * @return 結果ステータス
   */
  Status setRawText(const QString& itemName
                    , const char* text
                    , WORD len
                    , bool isSummary = true
                    );

protected:
  Database* pDb_;
  mutable NOTEHANDLE handle_;
  mutable NOTEID noteId_;
};

} // namespace ntlx

#endif // NTLX_NOTE_H
