﻿#include "ntlx/idtable.h"
#include "ntlx/note.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <idtable.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace ntlx
{

IDTable::iterator::iterator()
  : idTable_(nullptr)
  , id_(0)
  , isLast_(false)
{
}

NOTEID IDTable::iterator::operator *()
{
  return id_;
}

IDTable::iterator& IDTable::iterator::operator++()
{
  isLast_ = !IDScan(idTable_->handle_, id_ == 0, &id_);
  return *this;
}

IDTable::iterator IDTable::iterator::operator++(int)
{
  iterator it = *this;
  operator++();
  return it;
}

Note IDTable::iterator::getNote(Database &db) const
{
  return Note(db, id_);
}

IDTable::iterator::iterator(IDTable *pIdTable, NOTEID noteId, bool isLast)
  : idTable_(pIdTable)
  , id_(noteId)
  , isLast_(isLast)
{
}

bool operator==(const IDTable::iterator& lhs, const IDTable::iterator& rhs)
{
  if (lhs.isLast_ && rhs.isLast_)
    return true;
  else
    return lhs.id_ == rhs.id_;
}

bool operator!=(const IDTable::iterator& lhs, const IDTable::iterator& rhs)
{
  return !operator==(lhs, rhs);
}

IDTable::IDTable()
  : IStatus()
  , handle_(NULLHANDLE)
{
  lastStatus_ = IDCreateTable(sizeof(NOTEID), &handle_);
}

IDTable::~IDTable()
{
  if (handle_)
    IDDestroyTable(handle_);
}

IDTable::iterator IDTable::begin()
{
  if (handle_ == NULLHANDLE)
    return end();
  return ++iterator(this, 0);
}

IDTable::iterator IDTable::end()
{
  return iterator(this, 0, true);
}

bool IDTable::insert(NOTEID id)
{
  Q_ASSERT(handle_);

  BOOL b = false;
  lastStatus_ = IDInsert(handle_, id, &b);
  return b;
}

} // namespace ntlx
