﻿#include "ntlx/distinguishedname.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <dname.h>
#include <names.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace ntlx
{

DistinguishedName::DistinguishedName()
  : IStatus()
  , name_()
{
}

DistinguishedName::DistinguishedName(const Lmbcs& name)
  : IStatus()
  , name_()
{
  canonicalize(name);
}

DistinguishedName::DistinguishedName(const QString& name)
  : IStatus()
  , name_()
{
  canonicalize(Lmbcs::fromQString(name));
}

DistinguishedName::DistinguishedName(const DistinguishedName &other)
  : IStatus()
  , name_(other.name_)
{
}

DistinguishedName& DistinguishedName::operator=(const DistinguishedName& other)
{
  if (this == &other) return *this;
  name_ = other.name_;
  return *this;
}

Lmbcs DistinguishedName::abbreviated() const
{
  char abbreviate[MAXUSERNAME];
  WORD len;
  lastStatus_ = DNAbbreviate(
        0L
        , nullptr
        , name_.constData()
        , abbreviate
        , MAXUSERNAME
        , &len
        );
  if (lastStatus().success())
    return Lmbcs(abbreviate, len);
  return Lmbcs();
}

void DistinguishedName::canonicalize(const Lmbcs& name)
{
  char buffer[MAXUSERNAME];
  WORD len;
  lastStatus_ = DNCanonicalize(
        0L
        , nullptr
        , name.constData()
        , buffer
        , MAXUSERNAME
        , &len
        );
  if (lastStatus().success())
    name_ = Lmbcs(buffer, len);
  else
    name_ = Lmbcs();
}

DistinguishedName& DistinguishedName::operator=(const Lmbcs& name)
{
  canonicalize(name);
  return *this;
}

DistinguishedName& DistinguishedName::operator=(const QString& name)
{
  canonicalize(Lmbcs::fromQString(name));
  return *this;
}

bool operator==(const DistinguishedName& lhs, const DistinguishedName& rhs)
{
  return lhs.name_ == rhs.name_;
}

bool operator!=(const DistinguishedName& lhs, const DistinguishedName& rhs)
{
  return !operator==(lhs, rhs);
}

} // namespace ntlx
