﻿#include "ntlx/note.h"
#include "ntlx/database.h"
#include "ntlx/formula.h"
#include "ntlx/timedate.h"
#include "ntlx/item.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <osmem.h>
#include <stdnames.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace ntlx
{

Note::Note()
  : IStatus()
  , db_(nullptr)
  , id_(0)
  , handle_(NULLHANDLE)
{
}

Note::Note(Database& db, NOTEID id)
  : IStatus()
  , db_(&db)
  , id_(id)
  , handle_(NULLHANDLE)
{
}

Note::Note(const Note &other)
  : IStatus()
  , db_(other.db_)
  , id_(other.id_)
  , handle_(NULLHANDLE)
{
}

Note& Note::operator=(const Note& other)
{
  if (this == &other) return *this;
  assign(other.id_, other.db_);
  return *this;
}

Note::~Note()
{
  close();
}

void Note::assign(NOTEID noteId, Database *pDatabase)
{
  if (id_ == noteId && db_ == pDatabase) return;

  close();
  id_ = noteId;
  db_ = pDatabase;
}

Note& Note::open(WORD openFlags)
{
  if (handle_ == NULLHANDLE)
  {
    Q_ASSERT(db_ != nullptr);
    Q_ASSERT(db_->handle_ != NULLHANDLE);
    lastStatus_ = NSFNoteOpen(
          db_->handle_
          , id_
          , openFlags
          , &handle_
          );
  }
  return *this;
}

Note& Note::close()
{
  if (handle_ != NULLHANDLE)
  {
    lastStatus_ = NSFNoteClose(handle_);
    handle_ = NULLHANDLE;
  }
  return *this;
}

QString Note::getShortText(const QString &itemName)
{
  char buffer[256];

  if (open().lastStatus().failure())
    return QString();

  WORD len = NSFItemGetText(
        handle_
        , Lmbcs::fromQString(itemName).constData()
        , buffer
        , sizeof(buffer)
        );
  Lmbcs text(buffer, len);
  text.replace('\0', '\n');
  return text.toQString();
}

TimeDate Note::getTimeDate(const QString &itemName)
{
  TimeDate td;

  if (open().lastStatus().failure())
    return td;

  if (!NSFItemGetTime(
        handle_
        , Lmbcs::fromQString(itemName).constData()
        , &td.value_))
    td = TimeDate();
  return td;
}

QList<TimeDate> Note::getTimeDateList(const QString &itemName)
{
  QList<TimeDate> list;
  if (open().lastStatus().failure())
    return list;

  Lmbcs lmbcsItem = Lmbcs::fromQString(itemName);
  BLOCKID bidItem, bidValue;
  WORD type;
  DWORD valueLen;
  lastStatus_ = NSFItemInfo(
        handle_
        , lmbcsItem.constData()
        , lmbcsItem.size()
        , &bidItem
        , &type
        , &bidValue
        , &valueLen
        );

  switch (lastStatus_.error())
  {
  case NOERROR:
  {
    switch (type)
    {
    case TYPE_TIME_RANGE:
    {
      char* pValue = OSLockBlock(char, bidValue) + sizeof(WORD);
      RANGE* pRange = reinterpret_cast<RANGE*>(pValue);
      USHORT count = pRange->ListEntries;
      pValue += sizeof(RANGE);
      for (USHORT i = 0; i < count; ++i)
      {
        TimeDate td(*reinterpret_cast<TIMEDATE*>(pValue));
        list.append(td);
        pValue += sizeof(TIMEDATE);
      }
      OSUnlockBlock(bidValue);
    }
      break;
    }
  }
    break;
  }
  return list;
}

double Note::getFloat(const QString &itemName, const double defaultValue)
{
  NUMBER num = 0.0;

  if (open().lastStatus().failure())
    return defaultValue;

  if (!NSFItemGetNumber(
        handle_
        , Lmbcs::fromQString(itemName).constData()
        , &num))
    num = defaultValue;
  return num;
}

long Note::getLongInt(const QString &itemName, const long defaultValue)
{

  if (open().lastStatus().failure())
    return defaultValue;

  return NSFItemGetLong(
        handle_
        , Lmbcs::fromQString(itemName).constData()
        , defaultValue);
}

Formula Note::getFormula(const QString &itemName)
{
  if (open().lastStatus().failure())
    return Formula();

  Lmbcs lmbcsItem = Lmbcs::fromQString(itemName);
  BLOCKID bidItem, bidValue;
  WORD type;
  DWORD valueLen;
  lastStatus_ = NSFItemInfo(
        handle_
        , lmbcsItem.constData()
        , lmbcsItem.size()
        , &bidItem
        , &type
        , &bidValue
        , &valueLen
        );
  switch (lastStatus_.error())
  {
  case NOERROR:
  {
    if (type == TYPE_FORMULA)
    {
      const char* pValue = OSLockBlock(const char, bidValue) + sizeof(WORD);
      Formula formula(pValue, static_cast<WORD>(valueLen - sizeof(WORD)));
      OSUnlockBlock(bidValue);
      return formula;
    }
  }
    break;

//  case ERR_ITEM_NOT_FOUND:
//    break;

//  default:
//    throw Exception(s);
  }
  return Formula();
}

QString Note::getWindowTitle()
{
  QString formName = getShortText(FIELD_FORM);
  Note formNote = db_->getDesignNote(formName, NOTE_CLASS_FORM);
  Formula formula = formNote.getFormula(ITEM_NAME_WINDOWTITLE);
  return formula.evaluate(*this);
}

Note& Note::getItems(QList<Item> &items)
{
  if (open().lastStatus().failure())
    return *this;

  lastStatus_ = NSFItemScan(handle_, callback_getItems, &items);

  return *this;
}

STATUS LNPUBLIC Note::callback_getItems(
    WORD /*spare*/
    , WORD flags
    , char* name
    , WORD nameLength
    , void* value
    , DWORD valueLength
    , void* ptr
    )
{
  QList<Item>* items = (QList<Item>*)ptr;
  QString qName = Lmbcs(name, nameLength).toQString();
  Item item(qName, value, valueLength, flags);
  items->append(item);
  return NOERROR;
}

} // namespace ntlx
