﻿#include "ntlx/pathset.h"
#include "ntlx/netpath.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <names.h>
#include <osfile.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace ntlx
{

PathSet::PathSet()
  : path_()
  , server_()
  , port_()
{
}

PathSet::PathSet(
    const QString& path
    , const QString& server
    , const QString& port
    )
  : path_(path)
  , server_(server)
  , port_(port)
{
}

PathSet::PathSet(const PathSet& other)
  : path_(other.path_)
  , server_(other.server_)
  , port_(other.port_)
{
}

PathSet& PathSet::operator=(const PathSet& other)
{
  if (this == &other) return *this;
  path_ = other.path_;
  server_ = other.server_;
  port_ = other.port_;
  return *this;
}

PathSet::~PathSet()
{
}

NetPath PathSet::toNetPath() const
{
  char buffer[MAXPATH];
  Status result = OSPathNetConstruct(
        port_.isEmpty() ? 0 : Lmbcs::fromQString(port_).constData()
        , Lmbcs::fromQString(server_).constData()
        , Lmbcs::fromQString(path_).constData()
        , buffer
        );
  return result.success() ? NetPath(buffer) : NetPath();
}

} // namespace ntlx
