﻿#include "ntlx/summarymap.h"
#include "ntlx/lmbcs.h"
#include "ntlx/number.h"
#include "ntlx/timedate.h"
#include <QDateTime>
//#include <QtDebug>

namespace ntlx
{

SummaryMap::SummaryMap()
  : match_()
  , map_()
{
}

SummaryMap::SummaryMap(const SummaryMap &other)
  : match_(other.match_)
  , map_(other.map_)
{
}

SummaryMap& SummaryMap::operator=(const SummaryMap& other)
{
  if (this == &other) return *this;
  match_ = other.match_;
  map_ = other.map_;
  return *this;
}

void SummaryMap::load(
    const SEARCH_MATCH* const pMatch
    , ITEM_TABLE* pItemTable
    )
{
  if (pMatch != nullptr)
    memcpy((char*)&match_, (char*)pMatch, sizeof(SEARCH_MATCH));

  ITEM_TABLE itemTable = *pItemTable++;
  ITEM* pItem = (ITEM*)pItemTable;
  char* pName = (char*)(pItem + itemTable.Items);

  for (USHORT i = 0; i < itemTable.Items; ++i, ++pItem)
  {
    Lmbcs name(pName, pItem->NameLength);
//    qDebug() << name.toQString() << pItem->ValueLength;

    char* pValue = pName + pItem->NameLength;
    map_.insert(name.toQString(), getValue(
             *(WORD*)pValue
             , pValue + sizeof(WORD)
             , pItem->ValueLength - sizeof(WORD)
             ));
    pName += pItem->NameLength + pItem->ValueLength;
  }
}

template<class T, typename S, typename P>
QVariant getRangeValue(char* ptr, const QString& typeName)
{
  RANGE* pRange = (RANGE*)ptr;
  S* pSingle = (S*)(pRange + 1);
  P* pPair = (P*)(pSingle + pRange->ListEntries);

  QList<QVariant> list;
  for (USHORT i = 0; i < pRange->ListEntries; ++i, ++pSingle)
  {
    T item(*pSingle);
    list.append(item.toVariant());
  }

  QList<QVariant> pairList;
  for (USHORT i = 0; i < pRange->RangeEntries; ++i, ++pPair)
  {
    T lower(pPair->Lower);
    T upper(pPair->Upper);
    QMap<QString, QVariant> pair;
    pair.insert("lower", lower.toVariant());
    pair.insert("upper", upper.toVariant());
    pairList.append(pair);
  }

  QMap<QString, QVariant> value;
  value.insert("list", list);
  value.insert("range", pairList);
  value.insert("type", typeName);
  return value;
}

QVariant SummaryMap::getValue(WORD type, char *ptr, USHORT len)
{
  if (ptr == nullptr) return QVariant();

  switch (type)
  {
  case TYPE_TEXT:
  {
    Lmbcs value(ptr, len);
    QString str = value.toQString();
    return nullToCR(str);
  }
    break;

  case TYPE_TEXT_LIST:
  {
    QList<QString> list;
    LIST* pListTable = (LIST*)ptr;

    USHORT* pSize = (USHORT*)(pListTable + 1);
    char* pValue = (char*)(pSize + pListTable->ListEntries);
    for (USHORT i = 0; i < pListTable->ListEntries; ++i, ++pSize)
    {
      Lmbcs value(pValue, *pSize);
      QString str = value.toQString();
      list.append(nullToCR(str));
      pValue += *pSize;
    }
    return QVariant(list);
  }
    break;

  case TYPE_NUMBER:
    return *(NUMBER*)ptr;

  case TYPE_NUMBER_RANGE:
    return getRangeValue<Number, NUMBER, NUMBER_PAIR>(ptr, "number");

  case TYPE_TIME:
  {
    TimeDate td;
    td.value_ = *(TIMEDATE*)ptr;
    return td.toQDateTime();
  }
    break;

  case TYPE_TIME_RANGE:
    return getRangeValue<TimeDate, TIMEDATE, TIMEDATE_PAIR>(ptr, "timedate");
  }
  return QVariant();
}

} // namespace ntlx
