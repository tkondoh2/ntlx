﻿#include "ntlx/dbinfo.h"

#include <QString>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfdb.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace ntlx
{

DbInfo::DbInfo()
  : Lmbcs()
{
}

DbInfo::DbInfo(const char *s, int len)
  : Lmbcs(s, len)
{
}

DbInfo::DbInfo(const DbInfo &other)
  : Lmbcs(other)
{
}

DbInfo& DbInfo::operator=(const DbInfo& other)
{
  if (this == &other) return *this;
  Lmbcs::operator=(other);
  return *this;
}

DbInfo::~DbInfo()
{
}

QString DbInfo::getTitle() const
{
  return getInfo(INFOPARSE_TITLE);
}

QString DbInfo::getCategories() const
{
  return getInfo(INFOPARSE_CATEGORIES);
}

QString DbInfo::getClass() const
{
  return getInfo(INFOPARSE_CLASS);
}

QString DbInfo::getDesignClass() const
{
  return getInfo(INFOPARSE_DESIGN_CLASS);
}

QString DbInfo::getInfo(WORD what) const
{
  char parsedData[NSF_INFO_SIZE];
  NSFDbInfoParse(
        const_cast<char*>(constData())
        , what
        , parsedData
        , NSF_INFO_SIZE - 1
        );
  return Lmbcs(parsedData).toQString();
}

} // namespace ntlx
