﻿#include "ntlx/number.h"
#include <QVariant>

namespace ntlx
{

Number::Number()
  : value_(0.)
{
}

Number::Number(const NUMBER &value)
  : value_(value)
{
}

Number::Number(const Number &other)
  : value_(other.value_)
{
}

Number& Number::operator=(const Number& other)
{
  if (this == &other) return *this;
  value_ = other.value_;
  return *this;
}

QString Number::toString() const
{
  return QString::number(value_);
}

QVariant Number::toVariant() const
{
  return QVariant(value_);
}

Number Number::fromQtData(NUMBER from)
{
  return Number(from);
}

Number Number::fromVariant(const QVariant &var)
{
  return Number(var.toDouble());
}

} // namespace ntlx
