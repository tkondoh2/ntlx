#ifndef NTLX_GLOBAL_H
#define NTLX_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(NTLX_LIBRARY)
#  define NTLXSHARED_EXPORT Q_DECL_EXPORT
#else
#  define NTLXSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // NTLX_GLOBAL_H
