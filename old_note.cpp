﻿#include "note.h"
#include "database.h"
#include "lmbcs.h"

namespace ntlx
{

Note::Note()
  : pDb_(nullptr)
  , handle_(NULLHANDLE)
  , noteId_(0)
{
}

Note::Note(Database &db, NOTEID noteId)
  : pDb_(&db)
  , handle_(NULLHANDLE)
  , noteId_(noteId)
{
}

Note::Note(const Note &other)
  : pDb_(other.pDb_)
  , handle_(NULLHANDLE)
  , noteId_(other.noteId_)
{
}

Note& Note::operator=(const Note& other)
{
  if (this == &other) return *this;
  assign(other.noteId_, other.pDb_);
  return *this;
}

Note::~Note()
{
  close();
}

void Note::assign(NOTEID noteId)
{
  if (noteId_ == noteId) return;
  close();
  noteId_ = noteId;
}

void Note::assign(NOTEID noteId, Database *pDb)
{
  if (noteId_ == noteId && pDb_ == pDb) return;
  close();
  noteId_ = noteId;
  pDb_ = pDb;
}

Status Note::open(WORD openFlags) const
{
  Status result;
  if (handle_ == NULLHANDLE)
  {
    Q_ASSERT(pDb_);
    Q_ASSERT(pDb_->handle_);
    result = NSFNoteOpen(pDb_->handle_, noteId_, openFlags, &handle_);
  }
  Q_ASSERT(handle_);
  return result;
}

Status Note::close() const
{
  Status result;
  if (handle_ != NULLHANDLE)
  {
    result = NSFNoteClose(handle_);
    handle_ = NULLHANDLE;
  }
  return result;
}

Status Note::update(WORD updateFlags)
{
  Status result;
  if (handle_ != NULLHANDLE)
  {
    result = NSFNoteUpdate(handle_, updateFlags);
    if (noteId_ == 0)
      getInfo(_NOTE_ID, &noteId_);
  }
  return result;
}

void Note::getInfo(WORD _note_xxx, void *pData, Status *status)
{
  Status result = open();
  if (result.failure())
  {
    if (status != nullptr) *status = result;
    return;
  }

  NSFNoteGetInfo(handle_, _note_xxx, pData);
}

bool Note::hasItem(const QString &itemName, Status *status)
{
  Status result = open();
  if (result.failure())
  {
    if (status != nullptr)
      *status = result;
    return result.success();
  }

  Lmbcs lItemName = Lmbcs::fromQString(itemName);
  result = NSFItemIsPresent(handle_, lItemName.constData(), lItemName.size());
  if (status != nullptr) *status = result;
  return result.success();
}

Status Note::setText(const QString &itemName
                     , const QString &text
                     , bool isSummary
                     )
{
  Lmbcs lText = Lmbcs::fromQString(text);
  lText.replace('\n', '\0');
  return setRawText(itemName
                    , lText.constData()
                    , (WORD)lText.size()
                    , isSummary
                    );
}

Status Note::setRawText(const QString &itemName
                        , const char* text
                        , WORD len
                        , bool isSummary
                        )
{
  Status result = open();
  if (result.failure()) return result;

  Lmbcs lItemName = Lmbcs::fromQString(itemName);
  result = NSFItemSetTextSummary(
        handle_
        , lItemName.constData()
        , text
        , len
        , isSummary
        );
  return result;
}

} // namespace ntlx
