﻿#include "ntlx/cd/other.h"
#include <QStringList>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <ods.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace ntlx
{
namespace cd
{

Other::Other(char** ppRecord)
  : Cd()
  , record_()
{
  int len = 0;
  switch (((uchar*)*ppRecord)[1])
  {
  case 0x00: // Long Signature
    len = ((LSIG*)*ppRecord)->Length;
    break;

  case 0xff: // Word Signature
    len = ((WSIG*)*ppRecord)->Length;
    break;

  default: // Byte Signature
    len = ((BSIG*)*ppRecord)->Length;
    break;
  }

  record_ = QByteArray(*ppRecord, len);
}

Other::Other(const Other& other)
  : Cd()
  , record_(other.record_)
{
}

Other& Other::operator=(const Other& other)
{
  if (this != &other)
    record_ = other.record_;
  return *this;
}

WORD Other::getSignature() const
{
  switch (((uchar*)record_.constData())[1])
  {
  case 0x00: return ((LSIG*)record_.constData())->Signature;
  case 0xff: return ((WSIG*)record_.constData())->Signature;
  }
  return ((BSIG*)record_.constData())->Signature;
}

WORD Other::odsLength() const
{
  return (WORD)(record_.length() + record_.length() % 2);
}

QString Other::toString() const
{
  QStringList list;
  for (auto it = record_.constBegin(); it != record_.constEnd(); ++it)
  {
    uchar b = (uchar)(*it);
    list.append(QString("%1").arg((uint)b, 4, 16, QChar('0')).right(2));
  }
  return list.join(" ");
}

} // namespace cd

} // namespace ntlx
