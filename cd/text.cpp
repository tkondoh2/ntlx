﻿#include "ntlx/cd/text.h"

#include "ntlx/lmbcs.h"

namespace ntlx
{
namespace cd
{

Text::Text(char** ppRecord)
  : Base<CDTEXT, _CDTEXT, SIG_CD_TEXT>(ppRecord)
  , value_()
{
  WORD len = record_.Header.Length - ODSLength(_CDTEXT);
  Lmbcs lmbcs(*ppRecord, len);
  value_ = lmbcs.toQString();
  nullToCR(value_);
}

Text::Text(const Text &other)
  : Base<CDTEXT, _CDTEXT, SIG_CD_TEXT>(other)
  , value_(other.value_)
{
}

Text& Text::operator=(const Text& other)
{
  Base<CDTEXT, _CDTEXT, SIG_CD_TEXT>::operator=(other);
  if (this != &other)
    value_ = other.value_;
  return *this;
}

QString Text::value() const
{
  return value_;
}

QString Text::toString() const
{
  return value();
}

} // namespace cd

} // namespace ntlx
